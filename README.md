This code is an implementation of the Galerkin finite element method for elliptic problems.  The formulation is explained in doc/report/AE_5301_Project_Report.pdf.  The problem is defined in setup.h.  It works for nonuniform source terms and nonuniform conductivity (in the case of steady-state conduction). Gaussian quadrature is used to perform the numerical integrations.  The Armadillo linear algebra library is used extensively.  The global stiffness matrix is a sparse matrix.  It has been verified to be second-order accurate.  Examples on how to apply the code to different problems are in the src directory.  Essentially everything is templated to allow for any type to be used for the calculations.  The code works for all cell types using degeneration (i.e., degenerate cells).  Open index.html in include/html to browse UML diagrams and code documentation.  Available boundary conditions include:
- Dirichlet (essential)
- Neumann (natural)
- Robin (mixed)

Important Notes:

- This code only reads UGRID formatted meshes.  It could easily be extended to other formats by writing a new method to import the mesh in the mesh class.
- A ugrid2tec converter is included in the tests subdirectory.  This converter works all types of 3D meshes.  It does not work for 2D meshes.
- The boundary markers are assumed to be ordered linearly.  That is, when the user applies a boundary condition, the index must be provided.  This index is used to reference the boundary in an STL vector.  Example:

Boundary numbers: 4,2,1,5
Boundary indices: 0,1,2,3

The user can see this mapping by calling the print_boundary_mapping method in the mesh class.  Here is an example of how to do so:

    solver<double> my_solver;
    my_solver.read_mesh("test.ugrid");
    my_solver.print_boundary_mapping();

A simple example showing how to apply Neumann boundary conditions is provided below:

    #include <vector>
    #include <initializer_list>
    #include "solver.h"

    int main() {

    	bool check_zero_nodes=false;
    	std::vector<double> V_infty({10.0,0.0,0.0});

    	solver<double> s;
    	s.read_input_file("input.inp");
    	s.print_boundary_mapping();
    	s.assemble();
    	s.apply_gauge_condition(1.0);
    	s.apply_neumann_bc(0,V_infty);
    	s.solve_system(check_zero_nodes);
    	s.write_solution("soln.dat");

    	return 0;

    }

Author: James Grisham
