/*
 * This file is part of gfem-hc-3d.
 * 
 * gfem-hc-3d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * gfem-hc-3d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gfem-hc-3d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file solver.h
 * \class solver
 * 
 * This class contains methods that are used to interact with all of the
 * other classes.  It essentially provides a user-interface to the code.
 * It is templated based on type.  Also, the stiffness matrix is a sparse
 * matrix.
 *
 * \author James Grisham
 * \date 05/22/2015
 */

#ifndef SOLVERHEADERDEF
#define SOLVERHEADERDEF

#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <string>
#include <initializer_list>
#include <memory>
#include "armadillo"
#include "setup.h"
#include "boundary.h"
#include "mesh.h"
#include "material.h"
#include "shape_fcns_3d.h"

// TODO: Need to add method for least-squares gradient calculation
// TODO: Need to use better constructor for the sparse matrix class

/***************************************************************\
 * Class definition                                            *
\***************************************************************/

template <typename T> 
class solver {

  public:
    solver() {};
    void read_mesh(const std::string mesh_file);
    void read_input_file(const std::string input_file);
    void assemble();
		void apply_dirichlet_bc(const int boundary_index, T val);
		void apply_neumann_bc(const int boundary_index, const std::vector<T>& flux);
    void sample_neumann_integrands(const std::vector<T>& flux, T weight, T xi, T eta, std::vector<int>& con, arma::Col<T>& Fneumann) const;
		void apply_gauge_condition(const T val);
		void apply_robin_bc(const int boundary_index, T h);
		void sample_robin_integrands(T h, T Tinf, T weight, T xi, T eta, std::vector<int>& con, arma::Mat<T>& Kconv, arma::Col<T>& Fconv) const;
    void solve_system(const bool check_nodes);
		void print_boundary_mapping() const;
    void write_solution(const std::string output_file) const;
    void write_solution_additional(const std::string output_file, const arma::Col<T>& data, const std::string data_name) const;
		arma::Col<T> get_solution_vector() const;
		std::vector< node<T> > get_nodes() const;
		std::vector< tensor_element<T> > get_elements() const;
		int get_num_elements() const;

		std::vector<material<T> > materials;  /**< STL vector of material objects*/

  private:
    mesh<T> grid;
    arma::SpMat<T> K;
    arma::Col<T> F;
    arma::Col<T> u;
		T h;
		T Tamb;

};

/***************************************************************\
 * Class implementation                                        *
\***************************************************************/

/**
 * The read_mesh method uses the private mesh member of the solver
 * class to call the mesh method read_ugrid.  Only UGRID files are 
 * supported.
 *
 * @param[in] mesh_file is a string which contains the file name of the mesh to be imported.
 */

template <typename T> void solver<T>::read_mesh(const std::string mesh_file) {
  grid.read_ugrid(mesh_file);
}

/**
 * The read_input_file method reads the input file that the user provides.  
 * After reading the input file, the method reads the mesh and the element
 * marker file.  Then, each a private member of each element object is set 
 * to point to the appropriate material object.
 *
 * @param[in] input_file is a string which contains the name of the input file to be read.
 */

template <typename T> void solver<T>::read_input_file(const std::string input_file) {

  // Opening file 
	std::string line;
	std::string token;
	std::string gridfile;
	std::string markerfile;
	std::istringstream iss;
	std::ifstream infile(input_file.c_str());
	T T_ambient;
	int local_marker;
	T local_k;
	T local_rho;
	T local_Cp;
	T local_Q;
  
  // Reading information
  if (infile.is_open()) {
#ifdef FILE_READ_DEBUG
		std::cout << "File " << input_file << " is open." << std::endl;
#endif
		while (getline(infile,line)) {

			// Checking to see if the line is a comment
			if (line.find("#")==std::string::npos) {

				// Getting grid file
				if (line.find("GRIDFILE")!=std::string::npos) {
					iss.str(line);
					iss >> token >> gridfile;
					iss.clear();
#ifdef FILE_READ_DEBUG
					std::cout << "Read " << token << " " << gridfile << std::endl;
#endif
				}

				// Getting file for element material markers
				if (line.find("MATFILE")!=std::string::npos) {
					iss.str(line);
					iss >> token >> markerfile;
					iss.clear();
				}

				// Getting ambient temperature
				if (line.find("TAMB")!=std::string::npos) {
					iss.str(line);
					iss >> token >> T_ambient;
					Tamb = T_ambient;
					iss.clear();
				}

				// Getting material properties
				if (line.find("MATERIAL")!=std::string::npos) {
					iss.str(line);
					iss >> token >> local_marker >> local_k >> local_rho >> local_Cp >> local_Q;
					iss.clear();

					// Creating material objects
					// materials.push_back(std::make_shared<material<T> >(material<T> (local_marker,local_k,local_rho,local_Cp,local_Q)));
					 materials.push_back(material<T>(local_marker,local_k,local_rho,local_Cp,local_Q));
	
#ifdef FILE_READ_DEBUG
					std::cout << token << " " << local_marker << " " << local_k << " " << local_rho << " " << local_Cp << " " << local_Q << std::endl;
					std::cout << "materials.size() = " << materials.size() << std::endl;
#endif

				}

				// Getting convective heat transfer coefficient
				if (line.find("HCONV")!=std::string::npos) {
					iss.str(line);
					iss >> token >> h;
					iss.clear();
				}

			}

		}
  }
  else {
    std::cerr << "\nERROR: Can't open input file named " << input_file << std::endl;
    std::cerr << "Exiting.\n" << std::endl;
    exit(-1);
  }

	infile.close();

	// Reading the mesh
	read_mesh(gridfile);

	// Reading the element marker file
	std::cout << "Reading element marker file named " << markerfile << std::endl;
	std::vector<int> elem_markers (grid.get_num_elements(),0);
	std::ifstream mfile(markerfile);
	int nmarkers;
	mfile >> nmarkers;
	if (nmarkers != grid.get_num_elements()) {
		std::cerr << "\nERROR: Number of markers in " << markerfile << " does not" << std::endl;
		std::cerr << "match the number of elements in the grid object." << std::endl;
		std::cerr << "Exiting." << std::endl;
		exit(-1);
	}
	int dummy;
	for (int i=0; i<nmarkers; i++) {
		mfile >> dummy;
		elem_markers[i] = dummy;
	}
	mfile.close();

	std::vector< std::shared_ptr<material<T> > > ptr_vec;
	for (unsigned int i=0; i<materials.size(); i++) {
		ptr_vec.push_back( std::make_shared<material<T> >(materials[i]) );
	}

	// Assigning the material properties to the elements in the mesh
	int eidx = 0;
	for (tensor_element<T> & e : grid.elements) {

		// Finding the correct material object
		for (unsigned int mn=0; mn<materials.size(); ++mn) {

			// Assigning it to the current element
			if (materials[mn].get_marker() == elem_markers[eidx]) {

				e.elem_material = ptr_vec[mn];

#ifdef MATCH_DEBUG
				std::cout << "Matched element " << eidx << " with the correct material." << std::endl;
				std::cout << "use_count = " << e.elem_material.use_count() << std::endl;
#endif
			}
		} 
		if (e.elem_material==NULL) {
			std::cerr << "\nERROR: Element material not identified." << std::endl;
			std::cerr << "element marker: " << elem_markers[eidx] << std::endl;
			std::cerr << "Exiting.\n" << std::endl;
			exit(-1);
		}
		eidx++;
	}

}

/**
 * The assemble method performs the numerical integration over each element
 * to determine the element stiffness and load vectors.  The element stiffness
 * matrix and load vector are then assembled into the global stiffness matrix 
 * which is a sparse matrix.
 */

template<typename T> void solver<T>::assemble() {

  std::cout << "\nAssembling the global stiffness matrix and load vector ..." << std::endl;

  // Initializing data structures
  std::vector<int> con;
  K = arma::SpMat<T> (grid.get_num_nodes(),grid.get_num_nodes());
  F = arma::zeros< arma::Col<T> > (grid.get_num_nodes());
  arma::Mat<T> Ke = arma::zeros< arma::Mat<T> > (8,8); // <-- specifically hexes
  arma::Col<T> Fe = arma::zeros< arma::Col<T> > (8);

  // Global assembly
  int numel = grid.get_num_elements();
  for (int en=0; en<numel; en++) {
    
#ifdef SOLVER_DEBUG
		std::cout << "Assembling element " << en << std::endl;
#endif

    // Getting element properties
    con =	grid.elements[en].get_connectivity();
    get_element_properties_8pt(grid.elements[en],grid.nodes,Ke,Fe);
    //get_element_properties_1pt(grid.elements[en],grid.nodes,Ke,Fe);

    // Adding them to the global matrix
    for (unsigned int i=0; i<Ke.n_rows; i++) {
      for (unsigned int j=0; j<Ke.n_cols; j++) {
        K(con[i],con[j]) += Ke(i,j);
      }
      F(con[i]) += Fe(i);
    }

  }

#ifdef SOLVER_DEBUG
  K.print("K = ");
  F.print("F = ");
#endif

}

/** 
 * The apply_dirichlet_bc method is for applying dirichlet boundary 
 * conditions to the sparse global stiffness matrix. This is currently 
 * done in a sort of inefficient way.  Each row is entirely zeroed out.
 * Armadillo checks for zero entries and removes them.  This introduces 
 * some unnecessary overhead.  This could be remedied by changing the 
 * constructor for the global stiffness matrix.  More specifically, the 
 * compressed sparse column format could be used.  
 *
 * @param[in] boundary_index a constant integer index which is used to identify which boundary to apply the BCs to.
 */

template <typename T> void solver<T>::apply_dirichlet_bc(const int boundary_index, T val) {

	std::cout << "Applying Dirichlet boundary condition to boundary " << boundary_index << std::endl;

	// Declaring variables
	std::vector< std::vector<int> > bcons;  // boundary connectivity

	// Getting the boundary 
	boundary<T> b(grid.get_boundary(boundary_index));
	bcons = b.get_boundary_connectivity();  // this is a vector of vectors of ints
	int nn = grid.get_num_nodes();

	for (std::vector<int> b : bcons) {
		for (int node_num : b) {

#ifdef BC_DEBUG
			std::cout << "Applying boundary condition to node " << node_num << " on boundary " << boundary_index << std::endl;
#endif

			// I'm doing it this way because the iterators are REALLY slow and
			// armadillo will actually remove all these zeros automatically.
			for (unsigned int idx=0; idx<nn; idx++) {
				K(node_num,idx) = (T) 0;
			}

			// Putting one on the diagonal and assigning the load
			K(node_num,node_num) = (T) 1;
			F(node_num) = val;
			
		}
	}

}

/**
  This method is used to sample the integrands for Neumann boundary conditions. 

  @param[in]  flux STL vector which contains the gradient of the solution on the boundary.
  @param[in]      weight the weight used for Gaussian quadrature.
  @param[in]      xi the first coordinate in the computational plane.
  @param[in]      eta the second coordinate in the computational plane.
  @param[in]      con STL vector which contains integer values which correspond to the global node numbers for the given element.
  @param[in,out]  Fneumann an Armadillo column vector which contains the contribution of the Neumann boundary conditions to the global load vector.
  */

template <typename T> void solver<T>::sample_neumann_integrands(const std::vector<T>& flux, T weight, T xi, T eta, std::vector<int>& con, arma::Col<T>& Fneumann) const {

	// Computing elements of the Jacobian
	T dxdxi  = (T) 0;
	T dydxi  = (T) 0;
	T dzdxi  = (T) 0;
	T dxdeta = (T) 0;
	T dydeta = (T) 0;
	T dzdeta = (T) 0;
	for (int i=0; i<4; i++) {
		dxdxi  += grid.nodes[con[i]].get_x()*dNdxi<T>(i,xi,eta,(T) -1.0);
		dydxi  += grid.nodes[con[i]].get_y()*dNdxi<T>(i,xi,eta,(T) -1.0);
		dzdxi  += grid.nodes[con[i]].get_z()*dNdxi<T>(i,xi,eta,(T) -1.0);
		dxdeta += grid.nodes[con[i]].get_x()*dNdeta<T>(i,xi,eta,(T) -1.0);
		dydeta += grid.nodes[con[i]].get_y()*dNdeta<T>(i,xi,eta,(T) -1.0);
		dzdeta += grid.nodes[con[i]].get_z()*dNdeta<T>(i,xi,eta,(T) -1.0);
	}

	// Finding the normal vector
	arma::Col<T> n;        // normal vector
	arma::Col<T> nhat(3);  // unit normal vector
	arma::Col<T> r1(3);    // first position vector
	arma::Col<T> r2(3);    // second position vector
	T dx1 = grid.nodes[con[1]].get_x() - grid.nodes[con[0]].get_x();
	T dy1 = grid.nodes[con[1]].get_y() - grid.nodes[con[0]].get_y();
	T dz1 = grid.nodes[con[1]].get_z() - grid.nodes[con[0]].get_z();
	T dx2 = grid.nodes[con[2]].get_x() - grid.nodes[con[1]].get_x();
	T dy2 = grid.nodes[con[2]].get_y() - grid.nodes[con[1]].get_y();
	T dz2 = grid.nodes[con[2]].get_z() - grid.nodes[con[1]].get_z();
	r1(0) = dx1;
	r1(1) = dy1;
	r1(2) = dz1;
	r2(0) = dx2;
	r2(1) = dy2;
	r2(2) = dz2;
	n = arma::cross(r1,r2);
	T mag = sqrt(n(0)*n(0) + n(1)*n(1) + n(2)*n(2));
	nhat = n*((T) 1)/mag;

	// Finding the equivalent Jacobian
	arma::Col<T> dxi_v(3);
	arma::Col<T> deta_v(3);
	dxi_v(0)  = dxdxi;
	dxi_v(1)  = dydxi;
	dxi_v(2)  = dzdxi;
	deta_v(0) = dxdeta;
	deta_v(1) = dydeta;
	deta_v(2) = dzdeta;
	T detJ = arma::dot(arma::cross(dxi_v,deta_v),nhat);

	// Checking for zero or negative determinant
	if (detJ <= (T) 0) {
		std::cerr << "\nERROR: det(J) = " << detJ << std::endl;
		std::cerr << "From sample_neumann_integrands()." << std::endl;
		std::cerr << "Exiting.\n" << std::endl;
		exit(-1);
	}

	// The nhat vector is set to negative so that it is the outward pointing unit normal
  arma::Col<T> arma_flux = arma::conv_to<arma::Col<T> >::from(flux);
  T normal_flux = arma::dot(arma_flux,-nhat);
	// Adding contributions
	for (int i=0; i<4; i++) {
    // This is assuming that k=1.  If not, it won't work.  Need to figure
    // out a way for the boundary elements to inherit the material properties
    // from the element.
		Fneumann[i] += weight*N(i,xi,eta,(T) -1.0)*normal_flux*detJ;
	}

}

/**
  This method is used to perform the numerical integration necessary to apply the
  Neumann boundary conditions.  After integrating on an element, the contribution 
  to the global load vector are assembled.

  @param[in]  boundary_index a constant integer index used to identify which boundary the BCs should be applied to.
  @param[in]  flux an STL vector which contains the gradient of the solution on the given boundary.

 */

template <typename T> void solver<T>::apply_neumann_bc(const int boundary_index, const std::vector<T>& flux) {

	std::cout << "Applying Neumann boundary condition to boundary " << boundary_index << std::endl;

	// Declaring variables
	std::vector< std::vector<int> > bcons;  // boundary connectivity
	arma::Col<T> F_n = arma::zeros<arma::Col<T> > (4);

	// Getting the boundary 
	boundary<T> b(grid.get_boundary(boundary_index));
	bcons = b.get_boundary_connectivity();  // this is a vector of vectors of ints

	// Gauss points (four point quadrature)
	std::vector<T> w(2);
	std::vector<T> gpts(2);
	gpts[0] = ((T) 1)/((T) sqrt(3.0));
	gpts[1] = -gpts[0];
	w[0] = 1.0;
	w[1] = 1.0;

	// Determining contributions for each boundary element
	for (std::vector<int> &bel : bcons) {

		// Performing numerical integration
		for (unsigned int i=0; i<gpts.size(); i++) {
			for (unsigned int j=0; j<gpts.size(); j++) {
				sample_neumann_integrands(flux,w[i]*w[j],gpts[i],gpts[j],bel,F_n);
			}
		}

		// Adding contributions to the global load vector
		for (int i=0; i<4; i++) {
			F(bel[i]) += F_n(i);
		}

		// Zeroing out the element load vector used to enforce bcs
		F_n.zeros();
	}

}

/**
  This method can be used to apply a gauge condition when solving an elliptic
  equation with Neumann boundary conditions.  It uses pseudorandom number 
  generation to pick a node number at which the solution will be set.  After
  finding a pseudorandom number, a brute force search is used to make sure that
  the selected node does not lie on a boundary.  This could be improved using
  a better STL container and binary search instead.

  @param[in]  val value that is used to set the solution at the pseudorandomly generated node.
  */

template <typename T> void solver<T>::apply_gauge_condition(const T val) {

	// Generating a pseudo-random index
	srand(time(NULL));
	unsigned int node_idx;
	rand_gen: node_idx = rand()%(grid.get_num_nodes());
	
	// Checking to make sure the node is not on the boundary using a brute-force
	// search
	for (unsigned int i=0; i<grid.boundaries.size(); i++) {
		for (unsigned int j=0; j<grid.boundaries[i].boundary_elements.size(); j++) {
			for (unsigned int k=0; k<grid.boundaries[i].boundary_elements[j].size(); k++) {
				if (node_idx==grid.boundaries[i].boundary_elements[j][k]) {
					std::cout << "Node " << node_idx << " is on the boundary." << std::endl;
					goto rand_gen;
				}
			}
		}
	}

}

/**
  The sample_robin_integrands is used for sampling the integrands for the 
  convective boundary conditions.

  @param[in]      h is the convection heat transfer coefficient.
  @param[in]      Tinf is the freestream temperature.
  @param[in]      weight the weight used for Gaussian quadrature.
  @param[in]      xi the first coordinate in the computational plane.
  @param[in]      eta the second coordinate in the computational plane.
  @param[in]      con STL vector which contains integer values which correspond to the global node numbers for the given element.
  @param[in,out]  Kconv an Armadillo matrix which contains the contribution of the Robin boundary conditions to the global stiffness matrix.
  @param[in,out]  Fconv an Armadillo column vector which contains the contribution of the Robin boundary conditions to the global load vector.
 */

template <typename T> void solver<T>::sample_robin_integrands(T h, T Tinf, T weight, T xi, T eta, std::vector<int>& con, arma::Mat<T>& Kconv, arma::Col<T>& Fconv) const {


	// Computing elements of the Jacobian
	T dxdxi  = (T) 0;
	T dydxi  = (T) 0;
	T dzdxi  = (T) 0;
	T dxdeta = (T) 0;
	T dydeta = (T) 0;
	T dzdeta = (T) 0;
	for (int i=0; i<4; i++) {
		dxdxi  += grid.nodes[con[i]].get_x()*dNdxi<T>(i,xi,eta,(T) -1.0);
		dydxi  += grid.nodes[con[i]].get_y()*dNdxi<T>(i,xi,eta,(T) -1.0);
		dzdxi  += grid.nodes[con[i]].get_z()*dNdxi<T>(i,xi,eta,(T) -1.0);
		dxdeta += grid.nodes[con[i]].get_x()*dNdeta<T>(i,xi,eta,(T) -1.0);
		dydeta += grid.nodes[con[i]].get_y()*dNdeta<T>(i,xi,eta,(T) -1.0);
		dzdeta += grid.nodes[con[i]].get_z()*dNdeta<T>(i,xi,eta,(T) -1.0);
	}

	// Finding the normal vector
	arma::Col<T> n;        // normal vector
	arma::Col<T> nhat(3);  // unit normal vector
	arma::Col<T> r1(3);    // first position vector
	arma::Col<T> r2(3);    // second position vector
	T dx1 = grid.nodes[con[1]].get_x() - grid.nodes[con[0]].get_x();
	T dy1 = grid.nodes[con[1]].get_y() - grid.nodes[con[0]].get_y();
	T dz1 = grid.nodes[con[1]].get_z() - grid.nodes[con[0]].get_z();
	T dx2 = grid.nodes[con[2]].get_x() - grid.nodes[con[1]].get_x();
	T dy2 = grid.nodes[con[2]].get_y() - grid.nodes[con[1]].get_y();
	T dz2 = grid.nodes[con[2]].get_z() - grid.nodes[con[1]].get_z();
	r1(0) = dx1;
	r1(1) = dy1;
	r1(2) = dz1;
	r2(0) = dx2;
	r2(1) = dy2;
	r2(2) = dz2;
	n = arma::cross(r1,r2);
	T mag = sqrt(n(0)*n(0) + n(1)*n(1) + n(2)*n(2));
	nhat = n*((T) 1)/mag;

	// Finding the equivalent Jacobian
	arma::Col<T> dxi_v(3);
	arma::Col<T> deta_v(3);
	dxi_v(0)  = dxdxi;
	dxi_v(1)  = dydxi;
	dxi_v(2)  = dzdxi;
	deta_v(0) = dxdeta;
	deta_v(1) = dydeta;
	deta_v(2) = dzdeta;
	T detJ = arma::dot(arma::cross(dxi_v,deta_v),nhat);

	// Checking for zero or negative determinant
	if (detJ <= (T) 0) {
		std::cerr << "\nERROR: det(J) = " << detJ << std::endl;
		std::cerr << "From sample_robin_integrands()." << std::endl;
		std::cerr << "Exiting.\n" << std::endl;
		exit(-1);
	}

	// Adding contributions
	for (int i=0; i<4; i++) {
		Fconv[i] += weight*h*Tinf*N(i,xi,eta,(T) -1.0)*detJ;
		for (int j=0; j<4; j++) {
			Kconv(i,j) += weight*h*N(i,xi,eta,(T) -1.0)*N(j,xi,eta,(T) -1.0)*detJ;
		}
	}

}

/**
  The apply_robin_bc method is for applying Robin or mixed-type boundary 
  conditions.  It does this by performing the numerical integration

  @param[in]  boundary_index a constant integer index used to identify which boundary the BCs should be applied to.
  @param[in]  hval the convection heat transfer coefficient.
 */

template <typename T> void solver<T>::apply_robin_bc(const int boundary_index, T hval) {

	std::cout << "Appling mixed boundary conditions to boundary " << boundary_index << std::endl;

	// Getting boundary info
	std::vector< std::vector<int> > bcons;
	boundary<T> b(grid.get_boundary(boundary_index));
	bcons = b.get_boundary_connectivity();

	// Creating matrices 
	arma::Mat<T> Kbc = arma::zeros< arma::Mat<T> > (4,4);
	arma::Col<T> Fbc = arma::zeros< arma::Col<T> > (4);

	// Gauss points (four point quadrature)
	std::vector<T> w(2);
	std::vector<T> gpts(2);
	gpts[0] = ((T) 1)/((T) sqrt(3.0));
	gpts[1] = -gpts[0];
	w[0] = 1.0;
	w[1] = 1.0;

	// Performing numerical integration and adding contributions to global matrix
	// and load vector
	for (std::vector<int> &bcon : bcons) {

		// Sampling the integrands at the Gauss points
		for (unsigned int i=0; i<gpts.size(); i++) {
			for (unsigned int j=0; j<gpts.size(); j++) {
				sample_robin_integrands(hval,Tamb,w[i]*w[j],gpts[i],gpts[j],bcon,Kbc,Fbc);
			}
		}

		// Adding contributions to the global stiffness matrix and load vector
		for (int i=0; i<4; i++) {
			for (int j=0; j<4; j++) {
				K(bcon[i],bcon[j]) += Kbc(i,j);
			}
			F(bcon[i]) += Fbc[i];
		}

		// Zeroing out Kbc and Fbc
		Kbc.zeros();
		Fbc.zeros();
	}

}

/**
 The print_boundary_mapping method prints the mapping between the
 boundary objects in the private member boundaries and the markers
 in the UGRID file.
 */

template <typename T> void solver<T>::print_boundary_mapping() const {
	grid.print_boundary_mapping();
}

/**
 The solve_system method calls the Armadillo spsolve function which uses
 SuperLU to factor the matrix into an upper triangular matrix and a lower 
 triangular matrix.  The resulting matrices are then used to directly 
 solve the system of equations.  There is one default argument that
 can be used to check the mesh for nodes that are not connected to any 
 element.

 @param[in] check_nodes optional boolean argument that can be used to check the mesh for nodes that aren't in the connectivity.
 */

template <typename T> void solver<T>::solve_system(const bool check_nodes=false) {
	
	// Checking the diagonal for zero terms
	if (check_nodes) {
		int nctr=0;
		for (unsigned int i=0; i<F.n_elem; i++) {
			if (K(i,i)== (T) 0) {
				K(i,i) = (T) 1;
				nctr++;
			}
		}
		std::cout << "solver::solve_system() method found " << nctr << " nodes that weren't connected to any element." << std::endl;
	}

	arma::spsolve(u,K,F);
}

/**
  The write_solution method takes a string as an input and writes the solution,
  along with the mesh, to an ASCII Tecplot file.  All elements are written as 
  febricks.

  @param[in]  output_file string which contains the name of the Tecplot file to be written.
 */

template <typename T> void solver<T>::write_solution(const std::string output_file) const {

	std::cout << "Writing solution to Tecplot file named " << output_file << "\n" << std::endl;

	// Opening the file
	std::ofstream tecfile;
	tecfile.open(output_file.c_str());
	tecfile.setf(std::ios_base::scientific);
	tecfile.precision(16);

	// Writing header
	tecfile << "title=\"solution\"\n";
	tecfile << "variables=\"x\",\"y\",\"z\",\"T\"\n";
	tecfile << "zone nodes=" << grid.get_num_nodes() << ", elements=" << grid.get_num_elements() << ", datapacking=point, zonetype=febrick\n";

	// Writing nodes
	int idx=0;
	for (node<T> n : grid.nodes) {
		tecfile << n.get_x() << " " << n.get_y() << " " << n.get_z() << " " << u(idx) << std::endl;
		idx++;
	}

	// Writing connectivity
	std::vector<int> con_tmp;
	for (tensor_element<T> e : grid.elements) {
		con_tmp = e.get_connectivity();
		for (int c : con_tmp) {
			tecfile << c+1 << " ";
		}
		tecfile << std::endl;
	}

	std::cout << "Done writing solution to file.\n" << std::endl;

}

/**
  The write_solution_additional method is used to write the solution along with
  another variable which is defined at each node.  This method was added so that
  the numerical solution and exact solution could be written to the same file.

  @param[in]  output_file string which contains the name of the Tecplot file to be written.
  @param[in]  data Armadillo column vector which contains the additional nodal data to be written.
  @param[in]  data_name string which contains the name of the additional data set.
 */

template <typename T> void solver<T>::write_solution_additional(const std::string output_file, const arma::Col<T>& data, const std::string data_name) const {

	std::cout << "Writing solution to Tecplot file named " << output_file << "\n" << std::endl;

	// Opening the file
	std::ofstream tecfile;
	tecfile.open(output_file.c_str());
	tecfile.setf(std::ios_base::scientific);
	tecfile.precision(16);

	// Writing header
	tecfile << "title=\"solution\"\n";
	tecfile << "variables=\"x\",\"y\",\"z\",\"soln\",\"" << data_name << "\"\n";
	tecfile << "zone nodes=" << grid.get_num_nodes() << ", elements=" << grid.get_num_elements() << ", datapacking=point, zonetype=febrick\n";

	// Writing nodes
	int idx=0;
	for (node<T> n : grid.nodes) {
		tecfile << n.get_x() << " " << n.get_y() << " " << n.get_z() << " " << u(idx) << " " << data(idx) << std::endl;
		idx++;
	}

	// Writing connectivity
	std::vector<int> con_tmp;
	for (tensor_element<T> e : grid.elements) {
		con_tmp = e.get_connectivity();
		for (int c : con_tmp) {
			tecfile << c+1 << " ";
		}
		tecfile << std::endl;
	}

	std::cout << "Done writing solution to file.\n" << std::endl;

}

/**
  The get_solution_vector method returns the solution vector.  The user must
  make sure that the linear system has been solved before calling this method.
  If the system hasn't been solved, this method will return an empty Armadillo
  column vector.

  @returns An Armadillo column vector which contains the solution.
 */

template <typename T> arma::Col<T> solver<T>::get_solution_vector() const {
	return u;
}

/**
  The get_nodes method returns an STL vector of nodes from the private member
  of the solver class called grid.  

  @returns An STL vector of node objects for the grid.
 */

template <typename T> std::vector< node<T> > solver<T>::get_nodes() const {
	return grid.nodes;
}

/**
  The get_num_elements method uses the solver class' private member grid,
  which is an object of type mesh, to call the appropriate method within
  the mesh class.

  @returns An integer value for the number of elements in the mesh.
 */

template <typename T> int solver<T>::get_num_elements() const {
	return grid.get_num_elements();
}

/**
  The get_elements method uses the solver class' private member grid,
  which is an object of type mesh, to return a private member of the mesh
  class.  This is not safe, but it is necessary if the user needs to access
  the element objects for post-processing (e.g., numerical integration). 

  @returns An STL vector of element objects.
 */

template <typename T> std::vector< tensor_element<T> > solver<T>::get_elements() const {
	return grid.elements;
}

#endif
