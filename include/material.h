/*
 * This file is part of gfem-hc-3d.
 * 
 * gfem-hc-3d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * gfem-hc-3d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gfem-hc-3d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file material.h
 * \class material
 *
 * This is a simple class that contains material properties.
 * It must be passed to the element class.  It is templated based
 * on type.  Material is a specific case.  This could be an abstract
 * base class from which problem dependent classes could be derived.
 *
 * \author James Grisham
 * \date 05/14/2015
 */

#ifndef MATERIALHEADERDEF
#define MATERIALHEADERDEF

/***************************************************************\
 * Class definition                                            *
\***************************************************************/

template <typename T> 
class material {

  public:
		material() {};
    material(int mat_marker, T conductivity,T density,T heat_cap,T heat_gen) : marker{mat_marker}, k{conductivity}, rho{density}, Cp{heat_cap}, Q{heat_gen} {};
		int get_marker() const;
    T get_k() const;
    T get_rho() const;
    T get_Cp() const;
    T get_Q() const;
		template <typename T1> friend std::ostream& operator<<(std::ostream& os, const material<T1>& input);

  private:
    int marker;
    T k;
    T rho;
    T Cp;
    T Q;

};

/***************************************************************\
 * Class implementation                                        *
\***************************************************************/

// Overloaded ostream operator
template <typename T1>
std::ostream& operator<<(std::ostream& os, const material<T1>& input) {
	os << "Material marker: " << input.get_marker() << std::endl;
	os << "k   = " << input.get_k() << std::endl;
	os << "rho = " << input.get_rho() << std::endl;
	os << "Cp  = " << input.get_Cp() << std::endl;
	os << "Q   = " << input.get_Q() << std::endl;
	return os;
}

/**
  This method is for getting the material marker.

  @returns the integer marker for the material.
  */
template <typename T> int material<T>::get_marker() const {
	return marker;
}

/**
  This method is for getting the conductivity.

  @returns the value for the conductivity.
  */
template <typename T> T material<T>::get_k() const {
  return k;
}

/**
  This method is for getting the material density.

  @returns the density.
  */
template <typename T> T material<T>::get_rho() const {
  return rho;
}

/**
  This method is for getting the heat capacitance.

  @returns the heat capacitance.
  */
template <typename T> T material<T>::get_Cp() const {
  return Cp;
}

/**
  This method is for getting the heat generation (source term).

  @returns the heat generation.
  */
template <typename T> T material<T>::get_Q() const {
  return Q;
}

#endif
