var searchData=
[
  ['sample_5fintegrands',['sample_integrands',['../setup_8h.html#a044be36e2d222a6f8e3d8662a2a092ec',1,'setup.h']]],
  ['sample_5fneumann_5fintegrands',['sample_neumann_integrands',['../classsolver.html#a918e941d31fb0cc7a742ac5f36fde3b0',1,'solver']]],
  ['sample_5frobin_5fintegrands',['sample_robin_integrands',['../classsolver.html#a22d302748443da6ade0424aa584d028c',1,'solver']]],
  ['set_5fconnectivity',['set_connectivity',['../classtensor__element.html#a5a58c164b79d6dafab83afdea5ac0c0a',1,'tensor_element']]],
  ['setup_2eh',['setup.h',['../setup_8h.html',1,'']]],
  ['shape_5ffcns_5f1d_2eh',['shape_fcns_1d.h',['../shape__fcns__1d_8h.html',1,'']]],
  ['shape_5ffcns_5f3d_2eh',['shape_fcns_3d.h',['../shape__fcns__3d_8h.html',1,'']]],
  ['solve_5fsystem',['solve_system',['../classsolver.html#ab611efe894cb36808bde2ea4112e1ed3',1,'solver']]],
  ['solver',['solver',['../classsolver.html',1,'solver&lt; T &gt;'],['../classboundary.html#ab259ad7748d8a9beff960d5f3eead3d2',1,'boundary::solver()'],['../classsolver.html#a82c950705a84f7a1b84178445f09f194',1,'solver::solver()']]],
  ['solver_2eh',['solver.h',['../solver_8h.html',1,'']]],
  ['source',['source',['../setup_8h.html#a516a8d7b6771bcc9561dbbdcb4b128e7',1,'setup.h']]]
];
