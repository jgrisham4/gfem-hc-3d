/*
 * This file is part of gfem-hc-3d.
 * 
 * gfem-hc-3d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * gfem-hc-3d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gfem-hc-3d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file boundary.h
 * \class boundary
 *
 * This is a class for holding boundary information.  It is templated
 * based on type.
 *
 * \author James Grisham
 * \date 05/02/2015
 */

#ifndef BOUNDARYHEADERDEF
#define BOUNDARYHEADERDEF

#include <vector>
#include "solver.h"

/***************************************************************\
 * Class definition                                            *
\***************************************************************/

template <typename T> 
class boundary {
  
	template <typename D> friend class solver;

  public:
    boundary(int b_marker, std::vector< std::vector<int> >& b_elements) : boundary_marker{b_marker}, boundary_elements{b_elements} {};
    std::vector< std::vector<int> > get_boundary_connectivity() const;
    int get_boundary_marker() const;

  private:
    int boundary_marker;
    std::vector< std::vector<int> > boundary_elements;
};

/***************************************************************\
 * Class implementation                                        *
\***************************************************************/

/**
  This method returns a vector of vectors of integers which represent the boundary connectivity.

  @returns A vector of vectors of integers.
  */

template <typename T> std::vector< std::vector<int> > boundary<T>::get_boundary_connectivity() const {
  return boundary_elements;
}

/**
  This method can be used to get the boundary marker. 

  @returns an integer which is the boundary marker.
  */
template <typename T> int boundary<T>::get_boundary_marker() const {
  return boundary_marker;
}

#endif
