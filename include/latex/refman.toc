\contentsline {chapter}{\numberline {1}Main Page}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Namespace Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Namespace List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}File Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}File List}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}Namespace Documentation}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}mapping Namespace Reference}{9}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Function Documentation}{9}{subsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.1}i\discretionary {-}{}{}\_\discretionary {-}{}{}ord}{9}{subsubsection.5.1.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.2}j\discretionary {-}{}{}\_\discretionary {-}{}{}ord}{9}{subsubsection.5.1.1.2}
\contentsline {subsubsection}{\numberline {5.1.1.3}k\discretionary {-}{}{}\_\discretionary {-}{}{}ord}{9}{subsubsection.5.1.1.3}
\contentsline {chapter}{\numberline {6}Class Documentation}{11}{chapter.6}
\contentsline {section}{\numberline {6.1}boundary$<$ T $>$ Class Template Reference}{11}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Detailed Description}{11}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Constructor \& Destructor Documentation}{12}{subsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.2.1}boundary}{12}{subsubsection.6.1.2.1}
\contentsline {subsection}{\numberline {6.1.3}Member Function Documentation}{12}{subsection.6.1.3}
\contentsline {subsubsection}{\numberline {6.1.3.1}get\discretionary {-}{}{}\_\discretionary {-}{}{}boundary\discretionary {-}{}{}\_\discretionary {-}{}{}connectivity}{12}{subsubsection.6.1.3.1}
\contentsline {subsubsection}{\numberline {6.1.3.2}get\discretionary {-}{}{}\_\discretionary {-}{}{}boundary\discretionary {-}{}{}\_\discretionary {-}{}{}marker}{12}{subsubsection.6.1.3.2}
\contentsline {subsection}{\numberline {6.1.4}Friends And Related Function Documentation}{12}{subsection.6.1.4}
\contentsline {subsubsection}{\numberline {6.1.4.1}solver}{12}{subsubsection.6.1.4.1}
\contentsline {section}{\numberline {6.2}material$<$ T $>$ Class Template Reference}{12}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Detailed Description}{13}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Constructor \& Destructor Documentation}{14}{subsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.2.1}material}{14}{subsubsection.6.2.2.1}
\contentsline {subsubsection}{\numberline {6.2.2.2}material}{14}{subsubsection.6.2.2.2}
\contentsline {subsection}{\numberline {6.2.3}Member Function Documentation}{14}{subsection.6.2.3}
\contentsline {subsubsection}{\numberline {6.2.3.1}get\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Cp}{14}{subsubsection.6.2.3.1}
\contentsline {subsubsection}{\numberline {6.2.3.2}get\discretionary {-}{}{}\_\discretionary {-}{}{}k}{14}{subsubsection.6.2.3.2}
\contentsline {subsubsection}{\numberline {6.2.3.3}get\discretionary {-}{}{}\_\discretionary {-}{}{}marker}{14}{subsubsection.6.2.3.3}
\contentsline {subsubsection}{\numberline {6.2.3.4}get\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Q}{14}{subsubsection.6.2.3.4}
\contentsline {subsubsection}{\numberline {6.2.3.5}get\discretionary {-}{}{}\_\discretionary {-}{}{}rho}{14}{subsubsection.6.2.3.5}
\contentsline {subsection}{\numberline {6.2.4}Friends And Related Function Documentation}{15}{subsection.6.2.4}
\contentsline {subsubsection}{\numberline {6.2.4.1}operator$<$$<$}{15}{subsubsection.6.2.4.1}
\contentsline {section}{\numberline {6.3}mesh$<$ T $>$ Class Template Reference}{15}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Detailed Description}{15}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Member Function Documentation}{16}{subsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.2.1}get\discretionary {-}{}{}\_\discretionary {-}{}{}boundary}{16}{subsubsection.6.3.2.1}
\contentsline {subsubsection}{\numberline {6.3.2.2}get\discretionary {-}{}{}\_\discretionary {-}{}{}num\discretionary {-}{}{}\_\discretionary {-}{}{}elements}{16}{subsubsection.6.3.2.2}
\contentsline {subsubsection}{\numberline {6.3.2.3}get\discretionary {-}{}{}\_\discretionary {-}{}{}num\discretionary {-}{}{}\_\discretionary {-}{}{}nodes}{16}{subsubsection.6.3.2.3}
\contentsline {subsubsection}{\numberline {6.3.2.4}print\discretionary {-}{}{}\_\discretionary {-}{}{}boundary\discretionary {-}{}{}\_\discretionary {-}{}{}mapping}{16}{subsubsection.6.3.2.4}
\contentsline {subsubsection}{\numberline {6.3.2.5}read\discretionary {-}{}{}\_\discretionary {-}{}{}ugrid}{16}{subsubsection.6.3.2.5}
\contentsline {subsubsection}{\numberline {6.3.2.6}write\discretionary {-}{}{}\_\discretionary {-}{}{}tecplot}{16}{subsubsection.6.3.2.6}
\contentsline {subsection}{\numberline {6.3.3}Member Data Documentation}{17}{subsection.6.3.3}
\contentsline {subsubsection}{\numberline {6.3.3.1}boundaries}{17}{subsubsection.6.3.3.1}
\contentsline {subsubsection}{\numberline {6.3.3.2}elements}{17}{subsubsection.6.3.3.2}
\contentsline {subsubsection}{\numberline {6.3.3.3}nodes}{17}{subsubsection.6.3.3.3}
\contentsline {section}{\numberline {6.4}node$<$ T $>$ Class Template Reference}{17}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Detailed Description}{18}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Constructor \& Destructor Documentation}{18}{subsection.6.4.2}
\contentsline {subsubsection}{\numberline {6.4.2.1}node}{18}{subsubsection.6.4.2.1}
\contentsline {subsubsection}{\numberline {6.4.2.2}node}{18}{subsubsection.6.4.2.2}
\contentsline {subsection}{\numberline {6.4.3}Member Function Documentation}{18}{subsection.6.4.3}
\contentsline {subsubsection}{\numberline {6.4.3.1}get\discretionary {-}{}{}\_\discretionary {-}{}{}x}{18}{subsubsection.6.4.3.1}
\contentsline {subsubsection}{\numberline {6.4.3.2}get\discretionary {-}{}{}\_\discretionary {-}{}{}y}{18}{subsubsection.6.4.3.2}
\contentsline {subsubsection}{\numberline {6.4.3.3}get\discretionary {-}{}{}\_\discretionary {-}{}{}z}{18}{subsubsection.6.4.3.3}
\contentsline {section}{\numberline {6.5}solver$<$ T $>$ Class Template Reference}{19}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}Detailed Description}{20}{subsection.6.5.1}
\contentsline {subsection}{\numberline {6.5.2}Constructor \& Destructor Documentation}{20}{subsection.6.5.2}
\contentsline {subsubsection}{\numberline {6.5.2.1}solver}{20}{subsubsection.6.5.2.1}
\contentsline {subsection}{\numberline {6.5.3}Member Function Documentation}{20}{subsection.6.5.3}
\contentsline {subsubsection}{\numberline {6.5.3.1}apply\discretionary {-}{}{}\_\discretionary {-}{}{}dirichlet\discretionary {-}{}{}\_\discretionary {-}{}{}bc}{20}{subsubsection.6.5.3.1}
\contentsline {subsubsection}{\numberline {6.5.3.2}apply\discretionary {-}{}{}\_\discretionary {-}{}{}gauge\discretionary {-}{}{}\_\discretionary {-}{}{}condition}{20}{subsubsection.6.5.3.2}
\contentsline {subsubsection}{\numberline {6.5.3.3}apply\discretionary {-}{}{}\_\discretionary {-}{}{}neumann\discretionary {-}{}{}\_\discretionary {-}{}{}bc}{20}{subsubsection.6.5.3.3}
\contentsline {subsubsection}{\numberline {6.5.3.4}apply\discretionary {-}{}{}\_\discretionary {-}{}{}robin\discretionary {-}{}{}\_\discretionary {-}{}{}bc}{21}{subsubsection.6.5.3.4}
\contentsline {subsubsection}{\numberline {6.5.3.5}assemble}{21}{subsubsection.6.5.3.5}
\contentsline {subsubsection}{\numberline {6.5.3.6}get\discretionary {-}{}{}\_\discretionary {-}{}{}elements}{21}{subsubsection.6.5.3.6}
\contentsline {subsubsection}{\numberline {6.5.3.7}get\discretionary {-}{}{}\_\discretionary {-}{}{}nodes}{21}{subsubsection.6.5.3.7}
\contentsline {subsubsection}{\numberline {6.5.3.8}get\discretionary {-}{}{}\_\discretionary {-}{}{}num\discretionary {-}{}{}\_\discretionary {-}{}{}elements}{21}{subsubsection.6.5.3.8}
\contentsline {subsubsection}{\numberline {6.5.3.9}get\discretionary {-}{}{}\_\discretionary {-}{}{}solution\discretionary {-}{}{}\_\discretionary {-}{}{}vector}{21}{subsubsection.6.5.3.9}
\contentsline {subsubsection}{\numberline {6.5.3.10}print\discretionary {-}{}{}\_\discretionary {-}{}{}boundary\discretionary {-}{}{}\_\discretionary {-}{}{}mapping}{22}{subsubsection.6.5.3.10}
\contentsline {subsubsection}{\numberline {6.5.3.11}read\discretionary {-}{}{}\_\discretionary {-}{}{}input\discretionary {-}{}{}\_\discretionary {-}{}{}file}{22}{subsubsection.6.5.3.11}
\contentsline {subsubsection}{\numberline {6.5.3.12}read\discretionary {-}{}{}\_\discretionary {-}{}{}mesh}{22}{subsubsection.6.5.3.12}
\contentsline {subsubsection}{\numberline {6.5.3.13}sample\discretionary {-}{}{}\_\discretionary {-}{}{}neumann\discretionary {-}{}{}\_\discretionary {-}{}{}integrands}{22}{subsubsection.6.5.3.13}
\contentsline {subsubsection}{\numberline {6.5.3.14}sample\discretionary {-}{}{}\_\discretionary {-}{}{}robin\discretionary {-}{}{}\_\discretionary {-}{}{}integrands}{22}{subsubsection.6.5.3.14}
\contentsline {subsubsection}{\numberline {6.5.3.15}solve\discretionary {-}{}{}\_\discretionary {-}{}{}system}{23}{subsubsection.6.5.3.15}
\contentsline {subsubsection}{\numberline {6.5.3.16}write\discretionary {-}{}{}\_\discretionary {-}{}{}solution}{23}{subsubsection.6.5.3.16}
\contentsline {subsubsection}{\numberline {6.5.3.17}write\discretionary {-}{}{}\_\discretionary {-}{}{}solution\discretionary {-}{}{}\_\discretionary {-}{}{}additional}{23}{subsubsection.6.5.3.17}
\contentsline {subsection}{\numberline {6.5.4}Member Data Documentation}{23}{subsection.6.5.4}
\contentsline {subsubsection}{\numberline {6.5.4.1}materials}{23}{subsubsection.6.5.4.1}
\contentsline {section}{\numberline {6.6}tensor\discretionary {-}{}{}\_\discretionary {-}{}{}element$<$ T $>$ Class Template Reference}{24}{section.6.6}
\contentsline {subsection}{\numberline {6.6.1}Detailed Description}{24}{subsection.6.6.1}
\contentsline {subsection}{\numberline {6.6.2}Constructor \& Destructor Documentation}{25}{subsection.6.6.2}
\contentsline {subsubsection}{\numberline {6.6.2.1}tensor\discretionary {-}{}{}\_\discretionary {-}{}{}element}{25}{subsubsection.6.6.2.1}
\contentsline {subsubsection}{\numberline {6.6.2.2}tensor\discretionary {-}{}{}\_\discretionary {-}{}{}element}{25}{subsubsection.6.6.2.2}
\contentsline {subsection}{\numberline {6.6.3}Member Function Documentation}{25}{subsection.6.6.3}
\contentsline {subsubsection}{\numberline {6.6.3.1}compute\discretionary {-}{}{}\_\discretionary {-}{}{}jacobian}{25}{subsubsection.6.6.3.1}
\contentsline {subsubsection}{\numberline {6.6.3.2}d\discretionary {-}{}{}N}{25}{subsubsection.6.6.3.2}
\contentsline {subsubsection}{\numberline {6.6.3.3}get\discretionary {-}{}{}\_\discretionary {-}{}{}connectivity}{26}{subsubsection.6.6.3.3}
\contentsline {subsubsection}{\numberline {6.6.3.4}get\discretionary {-}{}{}\_\discretionary {-}{}{}det\discretionary {-}{}{}J}{26}{subsubsection.6.6.3.4}
\contentsline {subsubsection}{\numberline {6.6.3.5}get\discretionary {-}{}{}\_\discretionary {-}{}{}material}{26}{subsubsection.6.6.3.5}
\contentsline {subsubsection}{\numberline {6.6.3.6}get\discretionary {-}{}{}\_\discretionary {-}{}{}node\discretionary {-}{}{}\_\discretionary {-}{}{}objects}{26}{subsubsection.6.6.3.6}
\contentsline {subsubsection}{\numberline {6.6.3.7}set\discretionary {-}{}{}\_\discretionary {-}{}{}connectivity}{26}{subsubsection.6.6.3.7}
\contentsline {subsection}{\numberline {6.6.4}Member Data Documentation}{26}{subsection.6.6.4}
\contentsline {subsubsection}{\numberline {6.6.4.1}elem\discretionary {-}{}{}\_\discretionary {-}{}{}material}{26}{subsubsection.6.6.4.1}
\contentsline {chapter}{\numberline {7}File Documentation}{27}{chapter.7}
\contentsline {section}{\numberline {7.1}boundary.\discretionary {-}{}{}h File Reference}{27}{section.7.1}
\contentsline {section}{\numberline {7.2}main.\discretionary {-}{}{}dox File Reference}{28}{section.7.2}
\contentsline {section}{\numberline {7.3}material.\discretionary {-}{}{}h File Reference}{28}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Function Documentation}{29}{subsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.1.1}operator$<$$<$}{29}{subsubsection.7.3.1.1}
\contentsline {section}{\numberline {7.4}mesh.\discretionary {-}{}{}h File Reference}{29}{section.7.4}
\contentsline {section}{\numberline {7.5}node.\discretionary {-}{}{}h File Reference}{30}{section.7.5}
\contentsline {section}{\numberline {7.6}setup.\discretionary {-}{}{}h File Reference}{31}{section.7.6}
\contentsline {subsection}{\numberline {7.6.1}Function Documentation}{32}{subsection.7.6.1}
\contentsline {subsubsection}{\numberline {7.6.1.1}conductivity}{32}{subsubsection.7.6.1.1}
\contentsline {subsubsection}{\numberline {7.6.1.2}get\discretionary {-}{}{}\_\discretionary {-}{}{}element\discretionary {-}{}{}\_\discretionary {-}{}{}properties\discretionary {-}{}{}\_\discretionary {-}{}{}1pt}{32}{subsubsection.7.6.1.2}
\contentsline {subsubsection}{\numberline {7.6.1.3}get\discretionary {-}{}{}\_\discretionary {-}{}{}element\discretionary {-}{}{}\_\discretionary {-}{}{}properties\discretionary {-}{}{}\_\discretionary {-}{}{}8pt}{32}{subsubsection.7.6.1.3}
\contentsline {subsubsection}{\numberline {7.6.1.4}sample\discretionary {-}{}{}\_\discretionary {-}{}{}integrands}{32}{subsubsection.7.6.1.4}
\contentsline {subsubsection}{\numberline {7.6.1.5}source}{32}{subsubsection.7.6.1.5}
\contentsline {section}{\numberline {7.7}shape\discretionary {-}{}{}\_\discretionary {-}{}{}fcns\discretionary {-}{}{}\_\discretionary {-}{}{}1d.\discretionary {-}{}{}h File Reference}{32}{section.7.7}
\contentsline {subsection}{\numberline {7.7.1}Function Documentation}{33}{subsection.7.7.1}
\contentsline {subsubsection}{\numberline {7.7.1.1}dpsi}{33}{subsubsection.7.7.1.1}
\contentsline {subsubsection}{\numberline {7.7.1.2}psi}{34}{subsubsection.7.7.1.2}
\contentsline {section}{\numberline {7.8}shape\discretionary {-}{}{}\_\discretionary {-}{}{}fcns\discretionary {-}{}{}\_\discretionary {-}{}{}3d.\discretionary {-}{}{}h File Reference}{34}{section.7.8}
\contentsline {subsection}{\numberline {7.8.1}Detailed Description}{36}{subsection.7.8.1}
\contentsline {subsection}{\numberline {7.8.2}Function Documentation}{36}{subsection.7.8.2}
\contentsline {subsubsection}{\numberline {7.8.2.1}d\discretionary {-}{}{}Ndeta}{36}{subsubsection.7.8.2.1}
\contentsline {subsubsection}{\numberline {7.8.2.2}d\discretionary {-}{}{}Ndxi}{36}{subsubsection.7.8.2.2}
\contentsline {subsubsection}{\numberline {7.8.2.3}d\discretionary {-}{}{}Ndzeta}{36}{subsubsection.7.8.2.3}
\contentsline {subsubsection}{\numberline {7.8.2.4}N}{37}{subsubsection.7.8.2.4}
\contentsline {section}{\numberline {7.9}solver.\discretionary {-}{}{}h File Reference}{37}{section.7.9}
\contentsline {section}{\numberline {7.10}tensor\discretionary {-}{}{}\_\discretionary {-}{}{}element.\discretionary {-}{}{}h File Reference}{38}{section.7.10}
\contentsline {chapter}{Index}{40}{section*.31}
