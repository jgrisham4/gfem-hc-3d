/*
 * This file is part of gfem-hc-3d.
 * 
 * gfem-hc-3d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * gfem-hc-3d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gfem-hc-3d.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file node.h
 * \class node
 *
 * This is a simple node class that holds coordinates for the nodes.
 * It has methods for getting the coordinates.  The coordinates can 
 * only be set through the constructor.  It is templated based on 
 * type.
 *
 * \author James Grisham
 * \date 05/02/2015
 */

#ifndef NODEHEADERDEF
#define NODEHEADERDEF

/***************************************************************\
 * Class definition                                            *
\***************************************************************/

template <typename T>
class node {
  
  public:
		node() {
      x = (T) 0;
      y = (T) 0;
      z = (T) 0;
    };
    node(const T& xpt, const T& ypt, const T& zpt) : x{xpt}, y{ypt}, z{zpt} {};
    T get_x() const;
    T get_y() const;
    T get_z() const;

  private:
    T x;  /**< x-coordinate of node. */
    T y;  /**< y-coordinate of node. */
    T z;  /**< z-coordinate of node. */

};

/***************************************************************\
 * Class implementation                                        *
\***************************************************************/

/**
  Method for getting the x-coordinate.

  @returns the x-coordinate.
  */
template <typename T> T node<T>::get_x() const {
  return x;
}

/**
  Method for getting the y-coordinate.

  @returns the y-coordinate.
  */
template <typename T> T node<T>::get_y() const {
  return y;
}

/**
  Method for getting the z-coordinate.

  @returns the z-coordinate.
  */
template <typename T> T node<T>::get_z() const {
  return z;
}

#endif
