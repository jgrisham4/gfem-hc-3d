/*
 * This file is part of gfem-hc-3d.
 * 
 * gfem-hc-3d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * gfem-hc-3d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gfem-hc-3d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file tensor_element.h
 * \class tensor_element
 *
 * This is a class for 3D tensor elements.  This class is 
 * templated based on type so that any arbitrary type can 
 * be passed in (e.g., complex).  The 3D shape functions are
 * used for computing the Jacobian.  Each element has specific material
 * properties that are set in the input file.  These material properties
 * are set by setting a private member which is a shared pointer.  
 * This shared pointer points to a material object which contains all
 * of the necessary information about the material.
 *
 * \author James Grisham
 * \date 05/22/2015
 */

#ifndef TENSORELEMENTHEADER
#define TENSORELEMENTHEADER

#include <iostream>
#include <cstdlib>
#include <vector>
#include <memory>
#include "node.h"
#include "material.h"
#include "shape_fcns_3d.h"
  
/***************************************************************\
 * Class definition                                            *
\***************************************************************/

template <typename T>
class tensor_element {

  public:
    tensor_element() {};
    tensor_element(const std::vector<int>& elem);
    std::vector<int> get_connectivity() const;
    void set_connectivity(const std::vector<int>& con);
    std::vector< node<T> > get_node_objects(const std::vector<node<T> >& nodes) const;
		T get_detJ(const T xi, const T eta, const T zeta, const std::vector<node<T> >& nodes) const;
    void compute_jacobian(const T xi, const T eta, const T zeta, const std::vector<node<T> >& nodes, arma::Mat<T>& Jinv, T& detJ, arma::Mat<T>& A) const;
    arma::Col<T> dN(const int i, const T xi, const T eta, const T zeta, const std::vector< node<T> >& nodes) const;
		std::shared_ptr<material<T> > elem_material;  /**< shared pointer which points to the material for the current element */
		std::shared_ptr<material<T> > get_material() const;

  private:
    std::vector<int> connectivity;  /**< vector of ints for connectivity information*/

};

/***************************************************************\
 * Class implementation                                        *
\***************************************************************/

/**
  This is a constructor for the tensor_element class.  

  @param[in]  elem is a std::vector<int> which contains the connectivity information for the element.
  */
template <typename T> tensor_element<T>::tensor_element(const std::vector<int>& elem) : connectivity{elem} {};

/**
  Method for getting the connectivity.

  @returns Returns an std::vector<int> of the connectivity information.
  */
template <typename T> std::vector<int> tensor_element<T>::get_connectivity() const {
  return connectivity;
}

/**
  Method for setting the connectivity.

  @param[in]  std::vector<int> which contains the connectivity information.
  */
template <typename T> void tensor_element<T>::set_connectivity(const std::vector<int>& con) {
  connectivity = con;
}

/**
  Method for getting the shared pointer which points to the material object
  for the current element.

  @returns A shared pointer which points to the material object for the element.
  */
template<typename T> std::shared_ptr<material<T> > tensor_element<T>::get_material() const {
	return elem_material;
}

/**
  Method for getting node objects for the current element.

  @param[in,out]  nodes is a std::vector of node objects for the entire mesh.
  */
template<typename T> std::vector< node<T> > tensor_element<T>::get_node_objects(const std::vector<node<T> >& nodes) const {

  std::vector< node<T> > node_coords(connectivity.size());
  int index = 0;
  for (int i: connectivity) {
    node_coords[index] = nodes[i];
    index++;
  }

  return node_coords;
}

/**
  Method for computing the Jacobian matrix.  The Jacobian is computed 
  using the approach given on page 147 of the book "The Finite Element 
  Method -- Its Basis and Fundamentals" by Zienkiewicz, et al.
  J = A*B  (i.e., the RHS of equation 5.11)

  @param[in]      xi first coordinate in the computational plane.
  @param[in]      eta second coordinate in the computational plane.
  @param[in]      zeta third coordinate in the computational plane.
  @param[in]      nodes is an STL vector of node objects.
  @param[in,out]  Jinv is an Armadillo matrix which holds the inverse of the Jacobian.
  @param[in,out]  detJ holds the Jacobian determinant.
  @param[in,out]  A is an Armadillo matrix which contains the derivatives of the shape functions wrt xi,eta,zeta.

 */
template <typename T> void tensor_element<T>::compute_jacobian(const T xi, const T eta, const T zeta, const std::vector< node<T> >& nodes, arma::Mat<T>& Jinv, T& detJ, arma::Mat<T>& A) const {

  // NOTE: the Jacobian is a function of xi,eta,zeta and x,y,z.
  // The x-y-z information is provided via the vector of nodes
  // and the element connectivity.  The xi, eta, zeta points are
  // required method arguments.

  // Declaring variables
  //T denom = (T) 8;
  A = arma::Mat<T> (3,8);
  arma::Mat<T> B(8,3);
  arma::Mat<T> J;
  std::vector< node<T> > X;

  // A matrix
	for (int i=0; i<8; i++) {
		A(0,i) = dNdxi<T>(i,xi,eta,zeta);
		A(1,i) = dNdeta<T>(i,xi,eta,zeta);
		A(2,i) = dNdzeta<T>(i,xi,eta,zeta);
	}

  // Getting node objects
  X = get_node_objects(nodes);
  
  // B matrix [x1 y1 z1]
  //          [x2 y2 z2]
  //          [.  .  . ]
  //          [.  .  . ]
  //          [.  .  . ]
  //          [x8 y8 z8]
  int i = 0;
  for (node<T> n : X) {
    B(i,0) = n.get_x();
    B(i,1) = n.get_y();
    B(i,2) = n.get_z();
    i++;
  }

  // Jacobian matrix
  J = A*B; // should be a 3x3

  // Finding determinant
  detJ = arma::det(J);

  // Making sure det(J) > 0
  if (detJ <= (T) 0) {
    std::cerr << "\nERROR: Negative cell volumes -- det(J) <= 0." << std::endl;
		std::cerr << "xi = " << xi << " eta = " << eta << " zeta = " << zeta << std::endl;
    std::cerr << "Exiting.\n" << std::endl;
    exit(-1);
  }

  // Finding inverse
  Jinv = arma::inv(J);

}

/**
  This method is for computing the derivatives dN_i/dx, dN_i/dy, dN_i/dz.
  It uses the Jacobian.  A std::vector is returned whose components are 
  dN_i/dx, dN_i/dy, dN_i/dz, respectively.

  @param[in]  i is an integer index.
  @param[in]  xi is the first coordinate in the computational plane.
  @param[in]  eta is the second coordinate in the computational plane.
  @param[in]  zeta is the third coordinate in the computational plane.
  @param[in]  nodes is an STL vector of node objects.
  @returns    an armadillo column vector of the derivatives.
  */

template <typename T> arma::Col<T> tensor_element<T>::dN(const int i, const T xi, const T eta, const T zeta, const std::vector< node<T> >& nodes) const {

  // Declaring variables
  arma::Col<T> dNdX;
  arma::Mat<T> dNdXi;  // This is a matrix of derivatives of the shape fcns
  arma::Mat<T> Jinv(3,3);
  T detJ;

  // Computing the Jacobian, the inverse of the Jacobian and the determinant.
  compute_jacobian(xi,eta,zeta,nodes,Jinv,detJ,dNdXi);

  // Computing the derivatives (equation 5.10 in Zienkiewicz book)
  dNdX = Jinv*dNdXi.col(i);

  return dNdX;

}

// This is a method for getting the determinant of the Jacobian matrix
template <typename T> T tensor_element<T>::get_detJ(const T xi, const T eta, const T zeta, const std::vector<node<T> >& nodes) const {

  // Declaring variables
  arma::Col<T> dNdX;
  arma::Mat<T> dNdXi;  // This is a matrix of derivatives of the shape fcns
  arma::Mat<T> Jinv(3,3);
  T detJ;

  // Computing the Jacobian, the inverse of the Jacobian and the determinant.
  compute_jacobian(xi,eta,zeta,nodes,Jinv,detJ,dNdXi);

	return detJ;
}

#endif
