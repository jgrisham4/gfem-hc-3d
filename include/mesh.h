/*
 * This file is part of gfem-hc-3d.
 * 
 * gfem-hc-3d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * gfem-hc-3d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gfem-hc-3d.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file mesh.h
 * \class mesh
 *
 * This is a class for handling the mesh.  It has methods for reading the
 * mesh and writing it in Tecplot format.  It is templated based on 
 * type.
 *
 * \author James Grisham
 * \date 05/02/2015
 */

#ifndef MESHHEADERDEF
#define MESHHEADERDEF

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <set>
#include <algorithm>
#include "armadillo"
#include "node.h"
#include "boundary.h"
#include "tensor_element.h"

/***************************************************************\
 * Class definition                                            *
\***************************************************************/

template <typename T> 
class mesh {

  public:
    int get_num_nodes() const;
    int get_num_elements() const;
    void read_ugrid(const std::string ugrid_file);
    void write_tecplot(const std::string tecplot_file) const;
		boundary<T> get_boundary(const int b_idx) const;
		void print_boundary_mapping() const;

    // These should be private, technically.  Need to add 
    // Methods to get them.
    std::vector< node<T> > nodes;  /**< vector of node objects */
    std::vector< boundary<T> > boundaries;  /**< vector of boundary objects */
    std::vector< tensor_element<T> > elements;  /**< vector of element objects */

  private:
    int nnodes, nelements, nbelements;
    int nquads, ntris, ntets, nhexs, nprisms, npyrs;
    std::vector< std::vector<int> > belems;

};

/***************************************************************\
 * Class implementation                                        *
\***************************************************************/

/**
  This method is for getting the number of nodes in the mesh.

  @returns the number of nodes in the mesh.
  */
template <typename T> int mesh<T>::get_num_nodes() const {
  return nnodes;
}

/**
  This method is for getting the number of elements.

  @returns the number of elements in the mesh.
  */
template <typename T> int mesh<T>::get_num_elements() const {
  return nelements;
}

/**
  This method is for reading UGRID meshes.  It currently only reads ASCII 
  UGRID files.  

  @param[in]  ugrid_file string which contains the name of the mesh file.
  */
template <typename T> void mesh<T>::read_ugrid(const std::string ugrid_file) {

  std::cout << "\nReading mesh from file named " << ugrid_file << std::endl;

  // Declaring some variables
  std::vector< std::vector<int> > elem;
  std::vector<int> quad_tmp(4,0);
  std::vector<int> hex_tmp(8,0);
  std::vector<int> surf_ids;
  
  // Opening file
  FILE* fp;
  fp = fopen(ugrid_file.c_str(), "r");
  if (fp==NULL) {
    std::cerr << "\nERROR in mesh::read_ugrid()." << std::endl;
    std::cerr << "Can't open " << ugrid_file << std::endl;
    std::cerr << "Exiting.\n" << std::endl;
    exit(-1);
  }

  // Reading header data
  fscanf(fp,"%d %d %d %d %d %d %d",&nnodes,&ntris,&nquads,&ntets,&npyrs,&nprisms,&nhexs);
  nelements = ntets + npyrs + nprisms + nhexs;
  nbelements = ntris + nquads;

  // Resizing vectors
  nodes.resize(nnodes);
  belems.resize(nbelements);
  surf_ids.resize(nbelements);
  elements.resize(nelements);

  // Reading the nodes
  double xdummy, ydummy, zdummy;
  for (int i=0; i<nnodes; i++) {
    fscanf(fp,"%lf %lf %lf",&xdummy,&ydummy,&zdummy);
    nodes[i] = node<T> ((T) xdummy, (T) ydummy, (T) zdummy);
  }

  // Reading the boundary tri connectivity
  int d1,d2,d3,d4,d5,d6,d7,d8;  // dummy indices
  for (int i=0; i<ntris; i++) {
    fscanf(fp,"%d %d %d",&d1,&d2,&d3);
    quad_tmp[0] = d1-1;
    quad_tmp[1] = d2-1;
    quad_tmp[2] = d3-1;
    quad_tmp[3] = d3-1;
    belems[i] = quad_tmp;;
  }

  // Reading the boundary quad connectivity
  for (int i=0; i<nquads; i++) {
    fscanf(fp,"%d %d %d %d",&d1,&d2,&d3,&d4);
    quad_tmp[0] = d1-1;
    quad_tmp[1] = d2-1;
    quad_tmp[2] = d3-1;
    quad_tmp[3] = d4-1;
    belems[i+ntris] = quad_tmp;
  }

  // Reading surface ids for the boundary faces
  for (int i=0; i<nbelements; i++) {
    fscanf(fp,"%d",&d1);
    surf_ids[i] = d1;
  }

	// Figuring out the unique values in the surf_ids vector
	std::set<int> s(surf_ids.begin(),surf_ids.end());
	std::vector<int> bm;
	bm.assign(s.begin(),s.end());
#ifdef BOUNDARY_DEBUG
	for (int bmi : bm) {
		std::cout << "unique boundary marker -->" << bmi << std::endl;
	}
#endif

	// Getting all of the boundary element indices for the unique markers
	std::vector<std::vector<int> > current_belems;
	for (int bm_i : bm) {
		for (int i=0; i<nbelements; i++) {
			if (surf_ids[i]==bm_i) {
				current_belems.push_back(belems[i]);
			}
		}
		boundaries.push_back(boundary<T>(bm_i,current_belems));
		current_belems.clear();
	}
	std::cout << "Found " << boundaries.size() << " boundaries. " << std::endl;

  // Creating boundary objects  -- This is the old way.  Assumes that
	// all the boundaries elements along with their markers are ordered 
	// in blocks.  That is, bmarker = 1 1 1 2 2 2 3 3 3 not
	// bmarker = 1 2 2 1 1 3 3 3.  Had to modify it for this reason.
	/*
  std::vector< std::vector<int> > current_belems;
  int current_marker = surf_ids[0];
  for (unsigned int i=0; i<belems.size(); i++) {
    if (surf_ids[i]!=current_marker) {
      boundaries.push_back(boundary<T>(current_marker,current_belems));
      current_marker = surf_ids[i];
      current_belems.clear();
    }
    current_belems.push_back(belems[i]);
  }
  boundaries.push_back(boundary<T>(current_marker,current_belems));
	*/

  // Reading tet elements
  for (int i=0; i<ntets; i++) {
    fscanf(fp,"%d %d %d %d",&d1,&d2,&d3,&d4);
    hex_tmp[0] = d1-1;
    hex_tmp[1] = d2-1;
    hex_tmp[2] = d3-1;
    hex_tmp[3] = d3-1;
    hex_tmp[4] = d4-1;
    hex_tmp[5] = d4-1;
    hex_tmp[6] = d4-1;
    hex_tmp[7] = d4-1;
    elements[i] = tensor_element<T>(hex_tmp);
  }

  // Reading pyramid elements
  for (int i=0; i<npyrs; i++) {
    fscanf(fp,"%d %d %d %d %d",&d1,&d2,&d3,&d4,&d5);
    hex_tmp[0] = d1-1;
    hex_tmp[1] = d2-1;
    hex_tmp[2] = d3-1;
    hex_tmp[3] = d4-1;
    hex_tmp[4] = d5-1;
    hex_tmp[5] = d5-1;
    hex_tmp[6] = d5-1;
    hex_tmp[7] = d5-1;
    elements[i+ntets] = tensor_element<T>(hex_tmp);
  }

  // Reading prism elements
  for (int i=0; i<nprisms; i++) {
    fscanf(fp,"%d %d %d %d %d %d",&d1,&d2,&d3,&d4,&d5,&d6);
    hex_tmp[0] = d1-1;
    hex_tmp[1] = d2-1;
    hex_tmp[2] = d3-1;
    hex_tmp[3] = d3-1;
    hex_tmp[4] = d4-1;
    hex_tmp[5] = d5-1;
    hex_tmp[6] = d6-1;
 		hex_tmp[7] = d6-1;
    elements[i+ntets+npyrs] = tensor_element<T>(hex_tmp);
  }

  // Reading hex elements
  for (int i=0; i<nhexs; i++) {
    fscanf(fp,"%d %d %d %d %d %d %d %d",&d1,&d2,&d3,&d4,&d5,&d6,&d7,&d8);
    hex_tmp[0] = d1-1;
    hex_tmp[1] = d2-1;
    hex_tmp[2] = d3-1;
    hex_tmp[3] = d4-1;
    hex_tmp[4] = d5-1;
    hex_tmp[5] = d6-1;
    hex_tmp[6] = d7-1;
    hex_tmp[7] = d8-1;
    elements[i+ntets+npyrs+nprisms] = tensor_element<T>(hex_tmp);
  }

  // Closing the file
  fclose(fp);
  std::cout << "Done reading the mesh." << std::endl;

  // Printing some debug info
#ifdef MESH_DEBUG

  // Printing node coordinates
  std::cout << "\nnodes: " << std::endl;
  for (unsigned int i=0; i<nodes.size(); i++) {
    std::cout << nodes[i].get_x() << " " << nodes[i].get_y() << " " << nodes[i].get_z() << std::endl;
  }

  // Printing surface element connectivity
  std::cout << "\nsurface element connectivity: " << std::endl;
  std::vector< std::vector<int> > con_tmp;
  for (boundary<T> b : boundaries) {
    con_tmp = b.get_boundary_connectivity();
    for (unsigned int i=0; i<con_tmp.size(); i++) {
      for (unsigned int j=0; j<con_tmp[i].size(); j++) {
        std::cout << con_tmp[i][j] << " ";
      }
      std::cout << "\n";
    }
  }

  // Printing element connectivity
  std::cout << "\nelement connectivity: " << std::endl;
  std::vector <int> tmp;
  for (tensor_element<T> e : elements) {
    tmp = e.get_connectivity();
    for (int con : tmp) {
      std::cout << con << " ";
    }
    std::cout << "\n";
  }
#endif

  // Printing summary information
  std::cout << "\n------------------------------------------------" << std::endl;
  std::cout << " Mesh summary " << std::endl;
  std::cout << "------------------------------------------------\n" << std::endl;
  std::cout << "Number of nodes              : " << nnodes << std::endl;
  std::cout << "Number of boundary triangles : " << ntris << std::endl;
  std::cout << "Number of boundary quads     : " << nquads << std::endl;
  std::cout << "Number of boundaries         : " << boundaries.size() << std::endl;
  std::cout << "Number of tetrahedral cells  : " << ntets << std::endl;
  std::cout << "Number of pyramid cells      : " << npyrs << std::endl;
  std::cout << "Number of prism cells        : " << nprisms << std::endl;
  std::cout << "Number of hexahedral cells   : " << nhexs << "\n" << std::endl;

}

/**
  This is a method for writing the mesh to a Tecplot file

  @param[in]  tecplot_file name of Tecplot file to be written.
  */
template <typename T> void mesh<T>::write_tecplot(const std::string tecplot_file) const {

  std::cout << "\nWriting mesh to Tecplot file named " << tecplot_file << "\n" << std::endl;

  // Opening the file
  std::ofstream tecfile;
  tecfile.open(tecplot_file.c_str());
  tecfile.setf(std::ios_base::scientific);
  tecfile.precision(16);
 
  // Writing header
  tecfile << "title=\"mesh\"\n";
  tecfile << "variables=\"x\",\"y\",\"z\"\n";
  tecfile << "zone nodes=" << nnodes << ", elements=" << nelements << ", datapacking=point, zonetype=febrick\n";

  // Writing nodes
  for (node<T> n : nodes) {
    tecfile << n.get_x() << " " << n.get_y() << " " << n.get_z() << std::endl;
  }

  // Writing connectivity information
  std::vector<int> con_tmp;
  for (tensor_element<T> e : elements) {
    con_tmp = e.get_connectivity();
    for (int c : con_tmp) {
      tecfile << c+1 << " ";
    }
    tecfile << std::endl;
  }

  tecfile.close();
  std::cout << "Done writing mesh to " << tecplot_file << "\n" << std::endl;

}

/**
  This is a method for getting the boundary

  @param[in]  b_idx integer index used to access the boundaries.
  */
template <typename T> boundary<T> mesh<T>::get_boundary(const int b_idx) const {

	return boundaries[b_idx];

}

/*
  This is a method for printing the mapping between the boundary indices 
  and the boundary markers.
  */
template <typename T> void mesh<T>::print_boundary_mapping() const {

	int counter = 0;
	std::cout << "\nBoundary index Boundary marker" << std::endl;
	std::cout << "----------------------------------" << std::endl;
	for (boundary<T> b : boundaries) {
		printf("%14d %15d\n",counter,b.get_boundary_marker());
		counter++;
	}

}

#endif
