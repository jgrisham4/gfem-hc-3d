/*
 * This file is part of gfem-hc-3d.
 * 
 * gfem-hc-3d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * gfem-hc-3d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with gfem-hc-3d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <cmath>
#include <iostream>
#include "armadillo"
#include "node.h"
#include "tensor_element.h"
#include "material.h"
#include "shape_fcns_3d.h"

/**
 * This header contains functions that are problem-specific.  To change the 
 * solver, change the setup within.  These functions are used within the 
 * solver class for assembly of the global stiffness matrix. 
 *
 * NOTE: This is where the problem is defined.  That is, the weak form is 
 *       defined in sample_integrands() and the source term and conductivity
 *       are set in source() and conductivity(), respectively.
 *
 * Author        : James Grisham
 * Date          : 05/03/2015
 * Revision Date :
 * 
 */

// Function for the nonuniform source term
template <typename T> 
T source(const T x, const T y, const T z) {
	return (T) 3.0*M_PI*M_PI*sin(M_PI*x)*sin(M_PI*y)*sin(M_PI*z);
}

// Function for the nonuniform thermal conductivity
template <typename T>
T conductivity(const T x, const T y, const T z) {
  //return (z > ((T) 2) ? (T) 1 : (T) 5);
	return (T) 1;
}

// Function for sampling the weak statement
// THIS IS WHERE THE WEAK STATEMENT IS DEFINED
template <typename T> 
void sample_integrands(const tensor_element<T>& elem, const T xi, const T eta, const T zeta, const std::vector< node<T> >& nodes, arma::Mat<T>& K_local, arma::Col<T>& F_local) {

  // Declaring variables
  arma::Col<T> dN_i;
  arma::Col<T> dN_j;
  //T kval;
  //T sigma;
  T xval = (T) 0;
  T yval = (T) 0;
  T zval = (T) 0;
  T Nval;

  // Getting nodes
  std::vector< node<T> > local_nodes;
  local_nodes = elem.get_node_objects(nodes);

  // Recovering the x-y-z values for the given xi-eta-zeta points
  for (int i=0; i<8; i++) {
    Nval = N(i,xi,eta,zeta);
    xval += Nval*local_nodes[i].get_x();
    yval += Nval*local_nodes[i].get_y();
    zval += Nval*local_nodes[i].get_z();
  }

#ifdef DEBUG
  std::cout << "Recovered x-y-z values: " << std::endl;
  std::cout << "x = " << xval << " y = " << yval << " z = " << zval << std::endl;
#endif

  // Determining thermal conductivity and the source term
	/* This is not necessary.  The conductivity and heat generation terms
		 are determined using the material object of the tensor_element class.
  kval = conductivity<T>(xval,yval,zval);
  sigma = source<T>(xval,yval,zval);
	*/

  // Getting the determinant of the Jacobian
  T detJ;
  detJ = elem.get_detJ(xi,eta,zeta,nodes);
#ifdef DETJ_DEBUG
	std::cout << "detJ = " << detJ << std::endl;
#endif

  // Assembling
  for (int i=0; i<8; i++) {
    dN_i = elem.dN(i,xi,eta,zeta,nodes);
    for (int j=0; j<8; j++) {
      dN_j = elem.dN(j,xi,eta,zeta,nodes);
      K_local(i,j) = elem.elem_material->get_k()*(dN_i(0)*dN_j(0) + dN_i(1)*dN_j(1) + dN_i(2)*dN_j(2))*detJ;  // <-- WEAK FORM
    }
    F_local(i) = elem.elem_material->get_Q()*N(i,xi,eta,zeta)*detJ;
  }

}

// Function for getting the element stiffness matrix and load vector using 8 point
// Gaussian quadrature
template <typename T>
void get_element_properties_8pt(const tensor_element<T>& elem, const std::vector<node<T> >& nodes, arma::Mat<T>& K_e, arma::Col<T>& F_e) {

  // IMPORTANT -- K_e and F_e must be initialized in the solver class
  // Overwriting old values in K_e and F_e
  K_e.zeros();
  F_e.zeros();

  // Declaring variables
  arma::Mat<T> K_local = arma::zeros< arma::Mat<T> > (8,8);
  arma::Col<T> F_local = arma::zeros< arma::Col<T> > (8);
  std::vector<T> xi(8);
  std::vector<T> eta(8);
  std::vector<T> zeta(8);

  // Gauss points
  // g = +/- 1/sqrt(3)
  T g = ((T) 1)/sqrt((T) 3);  
  xi[0] = -g;
  eta[0] = -g;
  zeta[0] = -g;
  xi[1] = g;
  eta[1] = -g;
  zeta[1] = -g;
  xi[2] = g;
  eta[2] = g;
  zeta[2] = -g;
  xi[3] = -g;
  eta[3] = g;
  zeta[3] = -g;
  xi[4] = -g;
  eta[4] = -g;
  zeta[4] = g;
  xi[5] = g;
  eta[5] = -g;
  zeta[5] = g;
  xi[6] = g;
  eta[6] = g;
  zeta[6] = g;
  xi[7] = -g;
  eta[7] = g;
  zeta[7] = g;
  
  // Forming the stiffness matrix
  for (int i=0; i<8; i++) {

    // Sampling the integrands at xi_i, eta_i and zeta_i
    sample_integrands(elem,xi[i],eta[i],zeta[i],nodes,K_local,F_local);

    // Adding the contributions from the present xi, eta, zeta
    K_e += K_local;
    F_e += F_local;
  }
  
}

// Function for getting the element stiffness matrix and load vector using 1 point
// Gaussian quadrature
template <typename T>
void get_element_properties_1pt(const tensor_element<T>& elem, const std::vector<node<T> >& nodes, arma::Mat<T>& K_e, arma::Col<T>& F_e) {

  // IMPORTANT -- K_e and F_e must be initialized in the solver class

  // Overwriting old values in K_e and F_e
  K_e.zeros();
  F_e.zeros();
	T w = (T) 8;

  // Declaring variables
  arma::Mat<T> K_local = arma::zeros< arma::Mat<T> > (8,8);
  arma::Col<T> F_local = arma::zeros< arma::Col<T> > (8);
	T xi = (T) 0; // T{}  <-- this calls the default constructor of type T
	T eta = (T) 0;
	T zeta = (T) 0;

  // Sampling the integrands at xi_i, eta_i and zeta_i
  sample_integrands(elem,xi,eta,zeta,nodes,K_local,F_local);

  // Adding the contributions from the present xi, eta, zeta
  K_e += K_local;
  F_e += F_local;

	// Multiplying by the weights
	K_e *= w;
	F_e *= w;
  
}
