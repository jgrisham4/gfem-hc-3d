\documentclass[11pt,letterpaper,oneside]{article}

\usepackage{palatino,avant,graphicx,color}
\usepackage[margin=1.25in]{geometry}
\usepackage{amsmath}
\usepackage{cite}
\usepackage{float}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{courier}
\usepackage{tikz}
\usepackage{calc}
\usepackage{ifthen}
\usepackage{import}
\usepackage[colorlinks=true]{hyperref}

% Setting graphics path
\graphicspath{{./images/}}

%***************************************************************
\begin{document}
\title{AE 5301 -- Finite Element Methods in Fluid Mechanics and Heat Transfer \\ {\Large \it Final Project Report}}

% Authors and dat
\author{James~Grisham}
\date{May 2015}
\maketitle

%===============================================================
\section{Introduction}
%===============================================================

The purpose of this project was to develop a general three-dimensional elliptic solver using the Galerkin finite element method.  The final code is templated, object-oriented C++ and is compatable with all element types (i.e., tetrahedra, prisms, pyramids, and hexahedra).  It is also able to handle non-uniform source terms and variable coefficients (e.g., orthotropic conductivity).  The Armadillo C++ library was used for sparse matrix capabilities and the linear system solver.  It is verified to be second-order accurate using trilinear elements.  The code is templated so that it can be used to compute the sensitivity of the solution to design variables using the semi-analytic complex variable method.  The code reads AFLR3 formatted grid files and writes ASCII Tecplot output.

In the next sections, the formulation is explained, the code is validated using an exact solution, and two set of results are presented.  The first set of results involves simulation of steady-state heat transfer for a ball grid array (a surface mounted chip carrier).  The entire geometry has 9 different materials, each with different material properties.  The second set of results is for the inviscid, irrotational flow of an incompressible fluid around a sphere.  

The code was documented using Doxygen.

%===============================================================
\section{Formulation}
%===============================================================

An example of the type of elliptic equation being solved is
\begin{equation}
	-\nabla \cdot (k \nabla T) = Q
\end{equation}
where $k=k(x,y,z)$, $Q=Q(x,y,z)$.  Defining the residual as
\begin{equation}
	\mathcal{R} = - \nabla \cdot(k \nabla T) - Q	
\end{equation}
Multiplying the residual by a weight function and integrating over a hexahedral element yields
\begin{equation}
	\int w (- \nabla \cdot (k \nabla T) - Q) d\Omega = \pi
\end{equation}
where $\pi$ is a functional.  This is the unsymmetric weak form.  Integration by parts will be applied so that only first-order derivatives are left.  In tensor notation, the equation is
\begin{equation}
	-\int_\Omega w (k T_{,i})_{,i} \, d\Omega -\int_\Omega w Q \, d\Omega = \pi
\end{equation}
The integration by parts in $n$-dimensions is
\begin{equation}
	\int_\Omega u v_{,i} \, d\Omega = \oint_\Gamma u v n_i \, d\Gamma - \int_\Omega v u_{,i} \, d\Omega
\end{equation}
Letting $u=w$ and $v = kT_{,i}$, the first integral can be written as
\begin{equation}
	\int_\Omega w (kT_{,i})_{,i} \, d\Omega = \oint k w T_{,i} n_i \, d\Gamma - \int_\Omega k w_{,i} T_{,i} \, d\Omega
\end{equation}
Written in vector notation, the result is
\begin{equation}
	-\int_\Omega w (\nabla \cdot (k\nabla T)) \, d\Omega = -\oint_\Gamma k w (\nabla T \cdot \hat{\mathbf{n}}) \, d\Gamma + \int_\Omega k (\nabla w \cdot \nabla T) \, d\Omega
\end{equation}
Now, the weak form becomes
\begin{equation}
	-\oint_\Gamma k w (\nabla T \cdot \hat{\mathbf{n}}) \, d\Gamma + \int_\Omega k (\nabla w \cdot \nabla T) \, d\Omega - \int_\Omega w Q \, d\Omega = \pi
\end{equation}

The weight function can be written as
\begin{equation}
	w(x,y,z) = \sum V_j N_j
\end{equation}
where the $N_j$ term represents the shape functions and $V_j$ represents a vector of unknown coefficients.  In the Galerkin method, the same basis that is used to form the shape functions for the weight function is also used to form the approximation function,
\begin{equation}
	\widetilde{T}(x,y,z) = \sum c_i N_i
\end{equation}
Assuming zero Neumann boundary conditions, and inserting the approximation and weight functions,
\begin{equation}
	\pi = \int_\Omega \left[ \left(\sum V_j \nabla N_j\right)  \cdot \left( \sum c_i \nabla N_i \right) \right] \, d\Omega - \int_\Omega Q \left( \sum V_j N_j \right) \, d\Omega
\end{equation}
There are two vectors of unknown coefficients (i.e., $V_j$ and $c_i$).  This problem can be solved by making the functional stationary with respect to the $V_j$s.  We are effectively trying to find a solution ($c_i$) that minimizes the functional with respect to the unknown coefficients.  Dropping the summation notation and taking the partial of the functional with respect to the $V_j$ terms yields
\begin{equation}
	\frac{\partial \pi}{\partial V_j} = \int_\Omega k \left( \nabla N_i \cdot \nabla N_j \right)\,d\Omega - \int_\Omega Q N_j \, d\Omega = 0
\end{equation}
The stiffness matrix is then
\begin{equation}
	K_{ij} = \int_\Omega k \left( \frac{\partial N_i}{\partial x} \frac{\partial N_j}{\partial x} + \frac{\partial N_i}{\partial y} \frac{\partial N_j}{\partial y} + \frac{\partial N_i}{\partial z} \frac{\partial N_j}{\partial z}  \right) \, d\Omega
\end{equation}
The load vector is 
\begin{equation}
	F_j = \int_\Omega Q N_j \, d\Omega
\end{equation}
Discretizing and performing the numerical integrations on all of the elements yields the following linear system:
\begin{equation}
	K_{ij} c_i = F_j
\end{equation}

Applying the same procedure with convection boundary conditions yields the following (in tensor notation):  
\begin{equation}
	\frac{\partial \pi}{\partial V_i} = \int_\Omega k N_{i,k} N_{j,k} c_j \, d\Omega + \oint_\Gamma h N_i N_j c_j \, d\Gamma = \int_\Omega N_i Q \, d\Omega + \oint_\Gamma h N_i T_\infty \, d\Gamma
\end{equation}
The surface integrals are somewhat complicated by the fact that we must integrate a two-dimensional shape in three dimensions.  They must be transformed to two-dimensional space and integrated there.  The Jacobian determinant can be written as
\begin{equation}
  \det(\mathbf{J})\equiv J = d\boldsymbol{\xi} \times d\boldsymbol{\eta} \cdot d\boldsymbol{\zeta}
\end{equation}
In tensor notation,
\begin{equation}
	J = \epsilon_{ijk} \, d\xi_j \, d\eta_k \, d\zeta_i
\end{equation}
where 
\begin{equation}
	d\xi_j = \frac{\partial N_j}{\partial \xi} \ \ \ d\eta_k = \frac{\partial N_k}{\partial \eta} \ \ \ d\zeta_i = \frac{\partial N_i}{\partial \zeta}
\end{equation}
The 2D element with arbitrary orientation in ${\bf R}^3$ can be transformed by setting the Jacobian to 
\begin{equation}
  J = \epsilon_{ijk} d\xi_j d\eta_k n_i
\end{equation}
where $n_i$ is the face unit normal vector.  This is essentially enforcing no variation in the $\zeta$ direction.

The shape functions for the trilinear element can be derived using a tensor product.  The 1-D linear shape function can be written as
\begin{equation}
  \psi(\xi) = \left\{ \frac{1-\xi}{2}, \frac{1+\xi}{2}\right\}
\end{equation}
For the trilinear element, the shape functions are defined as follows:
\begin{equation}
  N_a(\xi,\eta,\zeta) \equiv N_{ijk}(\xi,\eta,\zeta) = \psi_i(\xi) \, \psi_j(\eta) \, \psi_k(\zeta)
\end{equation}
Care must be taken to ensure that the node numbering matches with the numbering in the tensor product.  If not, the mapping must be defined.

%===============================================================
\section{Validation}
%===============================================================

Several meshes were generated for a unit cube (i.e., hexahedral, prisms, and tetrahedral).  The order of accuracy of the code was verified using the exact solution to the following equation:
\begin{equation}
	\nabla^2 T = -3 \pi \sin(\pi x) \sin(\pi y) \sin(\pi z)
\end{equation}
with the exact solution being
\begin{equation}
	T(x,y,z) = \sin(\pi x) \sin(\pi y) \sin(\pi z)
\end{equation}
The temperature on each exterior surface is set to zero using Dirichlet boundary conditions.  

The orders of accuracy are listed below.  They were computed using the $\mathcal{L}^2$ norm of the error.  That is,
\begin{equation}
	\mathcal{E}^2 = \int (T_\text{num} - T_\text{exact})^2 \, d\Omega
\end{equation}
The integral was evaluated using 3-point Gaussian quadrature (corresponds to 27 points in 3D).  The orders of accuracy are listed below:
\begin{verbatim}
Order of accuracy for hexes is 1.999946
Order of accuracy for prisms is 1.917929
Order of accuracy for tets is 2.146208
\end{verbatim}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{order_of_accuracy.eps}
  \caption{Plots showing the $\mathcal{L}^2$ norm of the error vs the inverse of the cube root of the number of elements.}
\end{figure}

The fine meshes used for each case are shown below:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{hex_mesh.png}
	\caption{Hexahedral mesh.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{prism_mesh.png}
	\caption{Prism mesh.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{tet_mesh.png}
	\caption{Tetrahedral mesh.}
\end{figure}

The solutions on each mesh are also shown below:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{hex_soln.png}
	\caption{Hexahedral solution.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{prism_soln.png}
	\caption{Prism solution.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{tet_soln.png}
	\caption{Tetrahedral solution.}
\end{figure}

%===============================================================
\section{Results}
%===============================================================

The code was applied to two problems, namely, steady-state heat conduction in a chip made up of 9 different materials and inviscid, irrotational flow of air around a sphere.  The heat conduction solution was validated using an existing code that has been thoroughly validated.

\subsection{Heat conduction}

\begin{figure}[H]
	\centering
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{hc_mesh.png}
    \caption{BGA mesh.}
  \end{subfigure} 
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{hc_mesh_close.png}
    \caption{BGA mesh--close-up.}
  \end{subfigure} 
\end{figure}

\begin{figure}[H]
	\centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{hc_contours_dennis.png}
    \caption{Contours of temperature using well-validated code.}
  \end{subfigure} 
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{hc_contours_grisham.png}
    \caption{Contours of temperature using the new code.}
  \end{subfigure} 
\end{figure}

\begin{figure}[H]
	\centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{hc_slice_dennis.png}
    \caption{Volume slice showing contours of temperature using well-validated code.}
  \end{subfigure} 
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{hc_slice_grisham.png}
    \caption{Volume slice showing contours of temperature using the new code.}
  \end{subfigure} 
\end{figure}


\subsection{Potential flow}

The code was also applied to potential flow around a sphere.  The freestream velocity was set to 10 m/s.  A hybrid mesh consisting of tetrahedral and prism cells was generated using Pointwise.  Prisms were extruded from the surface of the sphere.  

\begin{figure}[H]
	\centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{potential_mesh.png}
    \caption{Mesh for potential flow case}
  \end{subfigure} 
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{potential_mesh_close.png}
    \caption{Close-up of sphere inside mesh for potential flow case.}
  \end{subfigure} 
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{potential_streamlines.png}
  \caption{Slice showing contours of the $x$-component of velocity along with streamlines.}
\end{figure}

%===============================================================
\section{Future work}
%===============================================================

The code will be slightly modified so that sensitivities to design variables can be computed using the semi-analytic complex variable method.  Also, the current setup uses a direct solver.  Iterative solvers will be added.

%\bibliography{mesh_report}
%\bibliographystyle{plain}

%\subimport{../../include/latex/}{refman.tex}


%***************************************************************
\end{document}
