#include "solver.h"
#include <iostream>
#include <cstdlib>

int main() {

	double h = 20.0;
	bool check_zero_nodes=true;

	solver<double> s;
	s.read_input_file("input.inp");
	s.print_boundary_mapping();
	s.assemble();
	s.apply_robin_bc(1,h);
	s.apply_robin_bc(2,h);
	s.apply_robin_bc(3,h);
	s.solve_system(check_zero_nodes);
	s.write_solution("soln.dat");

	return 0;

}
