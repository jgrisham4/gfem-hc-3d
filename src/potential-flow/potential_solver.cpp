#include <vector>
#include <initializer_list>
#include "solver.h"

int main() {

	bool check_zero_nodes=false;
	std::vector<double> V_infty({10.0,0.0,0.0});

	solver<double> s;
	s.read_input_file("input.inp");
	s.print_boundary_mapping();
	s.assemble();
	s.apply_gauge_condition(1.0);
	s.apply_neumann_bc(0,V_infty);
	s.solve_system(check_zero_nodes);
	s.write_solution("soln.dat");

	return 0;

}
