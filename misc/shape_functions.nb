(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     16915,        526]
NotebookOptionsPosition[     15350,        470]
NotebookOutlinePosition[     15809,        488]
CellTagsIndexPosition[     15766,        485]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.6395927647692547`*^9, 3.639592773076235*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Psi]", "[", 
   RowBox[{"\[Xi]_", ",", "i_"}], "]"}], ":=", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{"i", "\[Equal]", "1"}], ",", 
    RowBox[{
     RowBox[{"1", "/", "2"}], " ", 
     RowBox[{"(", 
      RowBox[{"1", "-", "\[Xi]"}], ")"}]}], ",", 
    RowBox[{
     RowBox[{"1", "/", "2"}], 
     RowBox[{"(", 
      RowBox[{"1", " ", "+", " ", "\[Xi]"}], ")"}]}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.6395927784033546`*^9, 3.639592823968337*^9}, {
  3.6395930029211082`*^9, 3.639593018276185*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"NN", "[", 
   RowBox[{"\[Xi]_", ",", "\[Eta]_", ",", "\[Zeta]_"}], "]"}], ":=", " ", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"\[Psi]", "[", 
          RowBox[{"\[Xi]", ",", "i"}], "]"}], 
         RowBox[{"\[Psi]", "[", 
          RowBox[{"\[Eta]", ",", "j"}], "]"}], " ", 
         RowBox[{"\[Psi]", "[", 
          RowBox[{"\[Zeta]", ",", "k"}], "]"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", "2"}], "}"}]}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"j", ",", "1", ",", "2"}], "}"}]}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "1", ",", "2"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.639592871304524*^9, 3.639592972306079*^9}, {
  3.639593035332041*^9, 3.639593082751264*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ShFcn", "=", 
  RowBox[{
   RowBox[{"NN", "[", 
    RowBox[{"\[Xi]", ",", "\[Eta]", ",", "\[Zeta]"}], "]"}], "//", 
   "Flatten"}]}]], "Input",
 CellChangeTimes->{{3.639593085227417*^9, 3.639593097216774*^9}, {
  3.63959483451607*^9, 3.639594836723893*^9}, {3.639594887212462*^9, 
  3.639594889713381*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Xi]"}], ")"}]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.6395930977583637`*^9, 3.639594837320508*^9, {3.639594877826288*^9, 
   3.639594890291671*^9}, {3.6395960506078787`*^9, 3.639596096564453*^9}, 
   3.63959615461169*^9, 3.6395962031427193`*^9, 3.639596389713888*^9, {
   3.639596437502657*^9, 3.6395964525152903`*^9}, 3.640478611361658*^9, 
   3.6404839456830683`*^9, 3.6410576629962893`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Reordering so that it follows ugrid convention", "Section",
 CellChangeTimes->{{3.639594944359208*^9, 3.63959495427947*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[CapitalPsi]", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"ShFcn", "[", 
     RowBox[{"[", "1", "]"}], "]"}], ",", 
    RowBox[{"ShFcn", "[", 
     RowBox[{"[", "2", "]"}], "]"}], ",", 
    RowBox[{"ShFcn", "[", 
     RowBox[{"[", "4", "]"}], "]"}], ",", 
    RowBox[{"ShFcn", "[", 
     RowBox[{"[", "3", "]"}], "]"}], ",", 
    RowBox[{"ShFcn", "[", 
     RowBox[{"[", "5", "]"}], "]"}], ",", 
    RowBox[{"ShFcn", "[", 
     RowBox[{"[", "6", "]"}], "]"}], ",", 
    RowBox[{"ShFcn", "[", 
     RowBox[{"[", "8", "]"}], "]"}], ",", 
    RowBox[{"ShFcn", "[", 
     RowBox[{"[", "7", "]"}], "]"}]}], "}"}], " "}]], "Input",
 CellChangeTimes->{{3.6404840378804083`*^9, 3.640484044571375*^9}, {
  3.6404842142626*^9, 3.640484224236533*^9}, {3.641057654655499*^9, 
  3.6410576606053963`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Xi]"}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "8"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Zeta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "\[Eta]"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "\[Xi]"}], ")"}]}]}], "}"}]], "Output",
 CellChangeTimes->{3.641057663017665*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CForm", "[", 
  RowBox[{"ReplaceAll", "[", 
   RowBox[{
    RowBox[{"8", " ", 
     RowBox[{"\[CapitalPsi]", "[", 
      RowBox[{"[", "8", "]"}], "]"}], " ", "c"}], ",", 
    RowBox[{"Thread", "[", 
     RowBox[{"Rule", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"\[Xi]", ",", "\[Eta]", ",", "\[Zeta]"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"xi", ",", "eta", ",", "zeta"}], "}"}]}], "]"}], "]"}]}], 
   "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.639617095447137*^9, 3.639617151122575*^9}, {
   3.639617233703495*^9, 3.639617299197496*^9}, {3.639617335381074*^9, 
   3.639617381755507*^9}, 3.6396174170765467`*^9}],

Cell["c*(1 + eta)*(1 - xi)*(1 + zeta)", "Output",
 CellChangeTimes->{{3.639617139594129*^9, 3.639617151866465*^9}, {
   3.6396172341307898`*^9, 3.639617300181389*^9}, {3.639617336542341*^9, 
   3.6396173820375357`*^9}, 3.639617417510941*^9, 3.6404786118158407`*^9, 
   3.640483946028269*^9, 3.6410576630347557`*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Taking derivatives", "Section",
 CellChangeTimes->{{3.6395952132949123`*^9, 3.6395952272213497`*^9}, {
  3.639613331082859*^9, 3.6396133322574997`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"dPsidxi", "=", 
   RowBox[{"D", "[", 
    RowBox[{"\[CapitalPsi]", ",", "\[Xi]"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.639595020345932*^9, 3.639595050458352*^9}, {
   3.639595091817189*^9, 3.6395950951599693`*^9}, {3.639595157730529*^9, 
   3.639595165104558*^9}, 3.639595204229555*^9, {3.639596429693961*^9, 
   3.6395964298292513`*^9}, {3.639615936221244*^9, 3.639615937898923*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"dPsideta", "=", 
   RowBox[{"D", "[", 
    RowBox[{"\[CapitalPsi]", ",", "\[Eta]"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.6395950571466703`*^9, 3.639595066464223*^9}, {
   3.639595169786516*^9, 3.6395951730303*^9}, 3.6395952057394753`*^9, {
   3.639596432331756*^9, 3.6395964325059967`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"dPsidzeta", "=", 
   RowBox[{"D", "[", 
    RowBox[{"\[CapitalPsi]", ",", "\[Zeta]"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.639595071903926*^9, 3.639595078897341*^9}, {
  3.639595177248748*^9, 3.6395952070362988`*^9}, {3.639596435316066*^9, 
  3.639596435474825*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"dPsidxi", "=", 
   RowBox[{"ReplaceAll", "[", 
    RowBox[{"dPsidxi", ",", 
     RowBox[{"Thread", "[", 
      RowBox[{"Rule", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"\[Xi]", ",", "\[Eta]", ",", "\[Zeta]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"xi", ",", "eta", ",", "zeta"}], "}"}]}], "]"}], "]"}]}], 
    "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.639595284187386*^9, 3.639595361959704*^9}, {
  3.639595678757815*^9, 3.639595692774748*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"dPsideta", "=", 
   RowBox[{"ReplaceAll", "[", 
    RowBox[{"dPsideta", ",", 
     RowBox[{"Thread", "[", 
      RowBox[{"Rule", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"\[Xi]", ",", "\[Eta]", ",", "\[Zeta]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"xi", ",", "eta", ",", "zeta"}], "}"}]}], "]"}], "]"}]}], 
    "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.6395953678176203`*^9, 3.6395953747909317`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"dPsidzeta", "=", 
   RowBox[{"ReplaceAll", "[", 
    RowBox[{"dPsidzeta", ",", 
     RowBox[{"Thread", "[", 
      RowBox[{"Rule", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"\[Xi]", ",", "\[Eta]", ",", "\[Zeta]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"xi", ",", "eta", ",", "zeta"}], "}"}]}], "]"}], "]"}]}], 
    "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.639595380330282*^9, 3.6395953880380697`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CForm", "[", 
  RowBox[{"8", " ", "c", " ", 
   RowBox[{"dPsidzeta", "[", 
    RowBox[{"[", "8", "]"}], "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.639618273010229*^9, 3.639618289541431*^9}, {
  3.639618357501144*^9, 3.6396184381431837`*^9}}],

Cell["c*(1 + eta)*(1 - xi)", "Output",
 CellChangeTimes->{{3.6396182849985533`*^9, 3.6396182901005373`*^9}, {
   3.639618357880733*^9, 3.639618438704769*^9}, 3.6404786125228662`*^9, 
   3.640483946599386*^9, 3.641057663120522*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Writing the dervatives to a file", "Section",
 CellChangeTimes->{{3.639595413145746*^9, 3.6395954275715*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Length", "[", "dPsidxi", "]"}]], "Input",
 CellChangeTimes->{{3.639595946135756*^9, 3.639595959500743*^9}}],

Cell[BoxData["8"], "Output",
 CellChangeTimes->{
  3.639595959981037*^9, {3.639596050743312*^9, 3.639596096663053*^9}, 
   3.6395961547530737`*^9, 3.639596203327415*^9, 3.639596389831552*^9, {
   3.639596437636712*^9, 3.639596452637992*^9}, 3.6404786127085857`*^9, 
   3.6404839467752037`*^9, 3.641057663122974*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"Mat", "=", " ", 
   RowBox[{"{", 
    RowBox[{"dPsidxi", ",", "dPsideta", ",", "dPsidzeta"}], "}"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.639595653265169*^9, 3.639595671213503*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"AString", " ", "=", " ", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"ToString", "[", 
        RowBox[{"CForm", "[", 
         RowBox[{
          RowBox[{"Mat", "[", 
           RowBox[{"[", "i", "]"}], "]"}], "[", 
          RowBox[{"[", "j", "]"}], "]"}], "]"}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"j", ",", "1", ",", "8"}], "}"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "3"}], "}"}]}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.639595999624441*^9, 3.6395960686369658`*^9}, {
  3.6395961430537777`*^9, 3.6395961537501163`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"OpenWrite", ",", 
    RowBox[{"PageWidth", "\[Rule]", "Infinity"}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fstr", "=", 
   RowBox[{"OpenWrite", "[", "\"\<derivs.txt\>\"", "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Do", "[", 
  RowBox[{
   RowBox[{"Do", "[", 
    RowBox[{
     RowBox[{"WriteString", "[", 
      RowBox[{"fstr", ",", 
       RowBox[{"StringJoin", "[", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"ToString", "[", 
           RowBox[{"StringForm", "[", 
            RowBox[{"\"\<A(``,``) = \>\"", ",", 
             RowBox[{"i", "-", "1"}], ",", 
             RowBox[{"j", "-", "1"}]}], "]"}], "]"}], ",", 
          RowBox[{
           RowBox[{"AString", "[", 
            RowBox[{"[", "i", "]"}], "]"}], "[", 
           RowBox[{"[", "j", "]"}], "]"}], ",", "\"\<\\n\>\""}], "}"}], 
        "]"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"j", ",", "1", ",", "8"}], "}"}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"i", ",", "1", ",", "3"}], "}"}]}], 
  "]"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.6395952374097233`*^9, 3.639595244684141*^9}, {
  3.6395955949065533`*^9, 3.639595630144869*^9}, {3.63959572922094*^9, 
  3.639595738638781*^9}, {3.6395958402963667`*^9, 3.639595925873871*^9}, {
  3.639595970465938*^9, 3.639595978903413*^9}, {3.639596039181875*^9, 
  3.63959609564651*^9}, {3.639596449842393*^9, 3.639596450875589*^9}}]
}, Open  ]]
},
WindowSize->{796, 877},
WindowMargins->{{Automatic, 331}, {-9, Automatic}},
ShowSelection->True,
Magnification:>FEPrivate`If[
  FEPrivate`Equal[FEPrivate`$VersionNumber, 6.], 1.25, 1.25 Inherited],
FrontEndVersion->"8.0 for Linux x86 (64-bit) (November 7, 2010)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 144, 2, 36, "Input"],
Cell[704, 24, 548, 16, 36, "Input"],
Cell[1255, 42, 888, 24, 83, "Input"],
Cell[CellGroupData[{
Cell[2168, 70, 330, 8, 36, "Input"],
Cell[2501, 80, 2377, 72, 173, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4915, 157, 130, 1, 86, "Section"],
Cell[CellGroupData[{
Cell[5070, 162, 829, 22, 60, "Input"],
Cell[5902, 186, 2055, 67, 173, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7994, 258, 669, 17, 60, "Input"],
Cell[8666, 277, 315, 4, 52, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9030, 287, 158, 2, 86, "Section"],
Cell[9191, 291, 431, 8, 36, "Input"],
Cell[9625, 301, 337, 7, 36, "Input"],
Cell[9965, 310, 313, 7, 36, "Input"],
Cell[10281, 319, 520, 14, 60, "Input"],
Cell[10804, 335, 477, 13, 60, "Input"],
Cell[11284, 350, 477, 13, 60, "Input"],
Cell[CellGroupData[{
Cell[11786, 367, 270, 6, 36, "Input"],
Cell[12059, 375, 230, 3, 52, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[12338, 384, 115, 1, 86, "Section"],
Cell[CellGroupData[{
Cell[12478, 389, 131, 2, 36, "Input"],
Cell[12612, 393, 315, 5, 36, "Output"]
}, Open  ]],
Cell[12942, 401, 223, 6, 36, "Input"],
Cell[13168, 409, 668, 18, 60, "Input"],
Cell[13839, 429, 1495, 38, 190, "Input"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
