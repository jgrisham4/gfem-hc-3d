#include "basis.h"
#include <iostream>
#include <cstdlib>

int main() {
  
  // Declaring variables
  basis<double> my_basis;
  double val = 0.0;
  bool sum_test = false;
  bool lnode_test = false;
  bool rnode_test = false;

  // Tensor product for 2D element evaluated at xi = eta = 0
  for (int i=0; i<2; i++) {
    for (int j=0; j<2; j++) {
      //std::cout << my_basis.psi(i,0.0)*my_basis.psi(j,0.0) << " ";
      val+=my_basis.psi(i,0.0)*my_basis.psi(j,0.0);
    }
    //std::cout << std::endl;
  }

  if (val==1.0) {
    sum_test = true;
  }

  // Checking that the value at the nodes is correct
  if ((my_basis.psi(0,-1.0)==1.0)&&(my_basis.psi(1,-1.0)==0.0)) {
    lnode_test = true;
  }
  if ((my_basis.psi(0,1.0)==0.0)&&(my_basis.psi(1,1.0)==1.0)) {
    rnode_test = true;
  }

  // Checking tests
  if (sum_test==false) {
    std::cerr << "test_basis -- FAILED." << std::endl;
    std::cerr << "Failed test: sum_test" << std::endl;
    exit(-1);
  }
  else if (lnode_test==false) {
    std::cerr << "test_basis -- FAILED." << std::endl;
    std::cerr << "Failed test: lnode_test" << std::endl;
    exit(-1);
  }
  else if (rnode_test==false) {
    std::cerr << "test_basis -- FAILED." << std::endl;
    std::cerr << "Failed test: rnode_test" << std::endl;
    exit(-1);
  }
  else {
    std::cout << "test_basis -- Passed." << std::endl;
  }

  return 0;

}
