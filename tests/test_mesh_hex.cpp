#include <iostream>
#include <string>
#include "mesh.h"

int main() {

  mesh<double> grid;
  grid.read_ugrid("hex.ugrid");
  grid.write_tecplot("hex.tec");
  
  return 0;

}
