function plot_error

% Clearing workspace
clc,clear,close all

% Inputs
% imgpath='/home/grisham/codes/gfem-hc-3d/doc/report/images/';
imgpath='/home/James/codes/gfem-hc-3d/doc/report/images/';
dirs = {'hexes','prisms','tets'};
idx = 1;
figure('Position',[90 240 950 220])
fs = 12;

files = {'coarse','medium','fine'};
for id = 1:numel(dirs)
    accuracy(dirs{id},files)
end

% Iterating through directories and plotting

set(gcf,'PaperPositionMode','Auto')
print(gcf,'-depsc',[imgpath,'order_of_accuracy'])

% Function for determining the order of accuracy
    function accuracy(dir,file_list)
        
        d = zeros(numel(file_list),1);
        npts = d;
        for f_idx = 1:numel(file_list)
            data = importdata([dir,'/',file_list{f_idx},'.l2']);
            npts(f_idx) = data(1);
            d(f_idx) = data(2);
        end
        
        % Plotting on a log-log plot
        subplot(1,3,idx)
        plot(1./npts,d,'--sk','LineWidth',1.5,'MarkerFaceColor','g');
        set(gca,'XScale','Log','YScale','Log');
        xlabel('$1/N$','Interpreter','LaTeX','FontSize',fs)
        ylabel('$\mathcal{L}^2$ norm of error','Interpreter','LaTeX',...
            'FontSize',fs)
        title(dir,'Interpreter','LaTeX','FontSize',fs)
        hold on
        
        % Determining slope
        A = zeros(numel(npts),2);
        A(:,1) = 1;
        A(:,2) = log10((1./npts).^(1/3));
        b = log10(d);
        xstar = (A.'*A)\A.'*b;
        fprintf('Order of accuracy for %s is %f\n',dir,xstar(2));
        set(gca,'XLim',[0.999e-5 1e-3],'YLim',[1e-4 1e-2])
        %plot(1./npts,10.^(2*log10(1./npts)+d(end)),'-k','LineWidth',1.5)
        idx = idx + 1;
        
    end

end
