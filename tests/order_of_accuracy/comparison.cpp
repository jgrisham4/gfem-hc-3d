#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include "solver.h"
#include "node.h"
#include "tensor_element.h"
#include "armadillo"

double exact(double x, double y, double z) {
	return sin(M_PI*x)*sin(M_PI*y)*sin(M_PI*z);
}

double sample_int(const tensor_element<double>& elem, const double xi, const double eta, const double zeta, const std::vector<node<double> >& nodes, const arma::Col<double>& soln) {

	// Declaring variables
	double xval = 0.0;
	double yval = 0.0;
	double zval = 0.0;
	double Nval;
	double integrand;
	double Ttilde = 0.0;
	double detJ;

	// Getting the nodes
	std::vector<node<double> > local_nodes;
	local_nodes = elem.get_node_objects(nodes);

	// Getting connectivity information
	std::vector<int> con = elem.get_connectivity();
	
	// Recovering the x-y-z values for the given xi-eta-zeta points and the
	// approximation function at the given xi-eta-zeta
	for (int i=0; i<8; i++) {
		Nval = elem.N(i,xi,eta,zeta);
		xval += Nval*local_nodes[i].get_x();
		yval += Nval*local_nodes[i].get_y();
		zval += Nval*local_nodes[i].get_z();
		Ttilde += soln(con[i])*Nval;
	}

	// Getting the Jacobian
	detJ = elem.get_detJ(xi,eta,zeta, nodes);

	// Computing the integrand
	integrand = pow(Ttilde - exact(xval,yval,zval),2)*detJ;

	return integrand;

}

int main(int argc, char **argv) {

	// Inputs are the grid file, the tecplot output file and the L2 error file
	if (argc!=4) {
		std::cerr << "\nUsage: " << argv[0] <<" [ugrid_file] [tecplot_file] [L2_error_file]\n" << std::endl;
		exit(1);
	}
	
	std::string gridname(argv[1]);
	std::string tecname(argv[2]);
	std::string l2name(argv[3]);

	// Solving Poisson's equation
  solver<double> test_solver;
  test_solver.read_mesh(gridname);
	test_solver.print_boundary_mapping();
  test_solver.assemble();
	test_solver.apply_dirichlet_bc(0,0.0);
	test_solver.solve_system();
	std::cout << "Done solving the system." << std::endl;
	//test_solver.write_solution("soln.dat");

	// Getting the solution vector and node coordinates
	arma::Col<double> soln = test_solver.get_solution_vector();
	arma::Col<double> exact_soln = arma::zeros< arma::Col<double> > (soln.n_elem);
	std::vector< node<double> > mesh_nodes = test_solver.get_nodes();
	int num_elements = test_solver.get_num_elements();
	std::vector< tensor_element<double> > elements = test_solver.get_elements();

	// Gauss points for 27-point quadrature
  std::vector<double> gpts(3);
	std::vector<double> w(3);

  // Gauss points
	gpts[0] = -sqrt(0.6);
	gpts[1] = 0.0;
	gpts[2] = sqrt(0.6);
	w[0] = 5.0/9.0;
	w[1] = 8.0/9.0;
	w[2] = 5.0/9.0;
	std::cout << "Performing numerical integration." << std::endl;
	

	// Computing the L2 norm
	double sum = 0.0;
	double norm;
	for (tensor_element<double> e : elements) {
		for (unsigned int i=0; i<gpts.size(); i++) {
			for (unsigned int j=0; j<gpts.size(); j++) {
				for (unsigned int k=0; k<gpts.size(); k++) {
					sum += w[i]*w[j]*w[k]*sample_int(e,gpts[i],gpts[j],gpts[k],mesh_nodes,soln);
				}
			}
		}
	}
	//norm = sqrt(sum/((double) num_elements));
	norm = sqrt(sum);
	
	// Writing exact and numerical solutions to file
	test_solver.write_solution_additional(tecname,exact_soln,"exact");

	// Writing L2 norm of error to file
	std::ofstream l2file;
	l2file.open(l2name);
	l2file.setf(std::ios_base::scientific);
	l2file.precision(12);
	//l2file << soln.n_elem << " " << norm << std::endl;
	l2file << num_elements << " " << norm << std::endl;
	l2file.close();

  return 0;

}
