#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include "solver.h"
#include "node.h"
#include "armadillo"

double exact(double x, double y, double z) {
	return sin(M_PI*x)*sin(M_PI*y)*sin(M_PI*z);
}

int main(int argc, char **argv) {

	// Inputs are the grid file, the tecplot output file and the L2 error file
	if (argc!=4) {
		std::cerr << "\nUsage: " << argv[0] <<" [ugrid_file] [tecplot_file] [L2_error_file]\n" << std::endl;
		exit(1);
	}
	
	std::string gridname(argv[1]);
	std::string tecname(argv[2]);
	std::string l2name(argv[3]);

	// Solving Laplace's equation
  solver<double> test_solver;
  test_solver.read_mesh(gridname);
	test_solver.print_boundary_mapping();
  test_solver.assemble();
	test_solver.apply_dirichlet_bc(0,0.0);
	test_solver.solve_system();
	//test_solver.write_solution("soln.dat");

	// Getting the solution vector and node coordinates
	arma::Col<double> soln = test_solver.get_solution_vector();
	arma::Col<double> exact_soln = arma::zeros< arma::Col<double> > (soln.n_elem);
	std::vector< node<double> > mesh_nodes = test_solver.get_nodes();
	int num_elements = test_solver.get_num_elements();

	// Computing the L2 norm
	double sum = 0.0;
	double norm;
	double x,y,z;
	for (unsigned int i=0; i<mesh_nodes.size(); i++) {
		x = mesh_nodes[i].get_x();
		y = mesh_nodes[i].get_y();
		z = mesh_nodes[i].get_z();
		exact_soln(i) = exact(x,y,z);
		sum += pow(soln(i) - exact_soln(i),2);
	}
	//norm = sqrt(sum/((double) num_elements));
	norm = sqrt(sum/((double) soln.n_elem));
	
	// Writing exact and numerical solutions to file
	test_solver.write_solution_additional(tecname,exact_soln,"exact");

	// Writing L2 norm of error to file
	std::ofstream l2file;
	l2file.open(l2name);
	l2file.setf(std::ios_base::scientific);
	l2file.precision(12);
	l2file << num_elements << " " << norm << std::endl;
	l2file.close();

  return 0;

}
