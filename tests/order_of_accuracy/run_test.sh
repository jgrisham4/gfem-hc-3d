#!/bin/bash

dirnames=("hexes" "prisms" "tets")
#dirnames=("hexes")
fnames=("coarse" "medium" "fine")
#fnames=("coarse" "medium" "fine" "ultrafine" "finest")

for d in ${dirnames[@]}; do
	echo "Changing to $d"
	cd $d
	for f in ${fnames[@]}; do
		echo "Running $f case."
		time ../comparison "$f.ugrid" "$f.tec" "$f.l2" &> "$f.log"
	done
	cd ..
done

matlab -nodisplay -r plot_error
