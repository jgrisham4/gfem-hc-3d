#include <iostream>
#include <memory>
#include <vector>
#include <string>

using namespace std;

/*-------------------------------------------------------*\
 * Class 1                                               * 
\*-------------------------------------------------------*/

class c1 {

	public:
		c1() {name="none";};
		c1(const string n) : name{n} {};
		string name;
		friend ostream& operator<<(ostream& os, const c1& input);

};

ostream& operator<<(ostream& os, const c1& input) {
	os << "My name is " << input.name;
	return os;
}

/*-------------------------------------------------------*\
 * Class 2                                               * 
\*-------------------------------------------------------*/

class c2 {

	public:
		vector<shared_ptr<c1> > c1vec;

};


/*-------------------------------------------------------*\
 * Main                                                  * 
\*-------------------------------------------------------*/

int main() {

	shared_ptr<c1> ptr1;
	shared_ptr<c1> ptr2;
	shared_ptr<c1> ptr3;
	{
		vector<shared_ptr<c1> > cl2;
		cl2.push_back(make_shared<c1>(c1("Bob")));
		cout << *cl2[0] << endl;
		cl2.push_back(make_shared<c1>(c1("Tom")));

		cout << "Inside scope, vector contains:" << endl;
		for (shared_ptr<c1> ptr : cl2) {
			cout << *ptr << endl;
		}
		ptr1 = cl2[0];
		ptr2 = cl2[0];
		ptr3 = cl2[0];
		
	}

	cout << "Outside scope:" << endl;
	cout << *ptr1 << endl;
	cout << ptr1.use_count() << endl;

	return 0;
}
