#include "solver.h"
#include <iostream>
#include <cstdlib>


class conv {
	
	public:
		conv(solver<double>& src) { local=&src;};

		void print_k() const {
			local->K.print("K = ");
		}
		void print_f() const {
			local->F.print("F = ");
		}
		solver<double> *local;
};

int main() {

	double T_inf = 25.0;
	double h = 20.0;

	solver<double> s;
	conv test = conv(s);
	s.read_input_file("input.inp");
	s.print_boundary_mapping();
	s.assemble();
	s.apply_robin_bc(0,h,T_inf);
	//s.apply_dirichlet_bc(0,100.0);
	s.solve_system();
	s.write_solution("soln.dat");

}
