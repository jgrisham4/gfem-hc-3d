#include <iostream>
#include <string>
#include "solver.h"

int main() {

  solver<double> test_solver;
  test_solver.read_mesh("hex.ugrid");
	test_solver.print_boundary_mapping();
  test_solver.assemble();
	test_solver.apply_dirichlet_bc(0,100.0);
	test_solver.solve_system();
	test_solver.write_solution("soln.dat");

  return 0;

}
