#include <iostream>
#include <string>
#include "solver.h"

int main() {

	// The Gauss points can be recovered using the equation for 
	// the transformation.  That is,
	//
	// xi(x) = (x - x_c)/a
	//
	// where x_c is the x-coordinate of the center of the element,
	// a is the length of the element divided by two.
	//
	// Using python to check the first point,
	// a = 0.25
	// x_c = 0.25
	// xi = -1/sqrt(3)
	// x = 0.105662

  solver<double> test_solver;
  test_solver.read_mesh("hex.ugrid");
	test_solver.print_boundary_mapping();
  test_solver.assemble();

	std::cout << "The x-value of the first Gauss point should be 0.105662" << std::endl;
  
  return 0;

}
