#include <iostream>
#include <string>
#include <cstdlib>
#include "mesh.h"

using namespace std;

int main(int argc, char **argv) {

  if (argc != 3) {
    cerr << "Usage: " << argv[0] << " [ugridfile] [tecplotfile]" << endl;
    exit(-1);
  }
  string in_file(argv[1]);
  string out_file(argv[2]);

  mesh<double> grid;
  grid.read_ugrid(in_file);
  grid.write_tecplot(out_file);
  
  return 0;

}
