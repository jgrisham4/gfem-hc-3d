#include <iostream>
#include <string>
#include "mesh.h"

int main() {

  mesh<float> grid;
  grid.read_ugrid("tet.ugrid");
  grid.write_tecplot("tet.tec");
	grid.print_boundary_mapping();
  
  return 0;

}
