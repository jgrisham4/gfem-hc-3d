#include <iostream>
#include "armadillo"

using namespace arma;
using namespace std;

int main() {

  SpMat<double> A(5,5);

	A(0,0) = 1.5;
  A(1,1) += 5.0;
  A(1,1) += 2.0;
  A(2,2) = 1.0;
	A(2,3) = 2.0;

	// This is an entire row of zeros.  Armadillo removes it automatically.
	A(4,0) = 0.0;
	A(4,1) = 0.0;
	A(4,2) = 0.0;
	A(4,3) = 0.0;
	A(4,4) = 0.0;

	typename SpMat<double>::row_iterator a = A.begin_row(2);
	SpMat<double>::row_iterator b = A.end_row(2);

	for(SpMat<double>::row_iterator i=a; i!=b; ++i) {
		cout << *i << endl;
		(*i) = 1.0;
	}
	

  A.print("A = ");
	
	A *= 2.0;
	
	A.print("2*A = ");


  return 0;

}
