#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include "solver.h"
#include "node.h"
#include "armadillo"

double exact(double x, double y, double z) {
	return sin(M_PI*x)*sin(M_PI*y)*sin(M_PI*z);
}

int main() {

	// Solving Laplace's equation
  solver<double> test_solver;
  test_solver.read_mesh("block_3d.ugrid");
	test_solver.print_boundary_mapping();
  test_solver.assemble();
	test_solver.apply_dirichlet_bc(0,0.0);
	test_solver.solve_system();
	//test_solver.write_solution("soln.dat");

	// Getting the solution vector and node coordinates
	arma::Col<double> soln = test_solver.get_solution_vector();
	arma::Col<double> exact_soln = arma::zeros< arma::Col<double> > (soln.n_elem);
	std::vector< node<double> > mesh_nodes = test_solver.get_nodes();

	// Computing the L2 norm
	double sum = 0.0;
	double norm;
	double x,y,z;
	for (unsigned int i=0; i<mesh_nodes.size(); i++) {
		x = mesh_nodes[i].get_x();
		y = mesh_nodes[i].get_y();
		z = mesh_nodes[i].get_z();
		exact_soln(i) = exact(x,y,z);
		sum += pow(soln(i) - exact_soln(i),2);
	}
	norm = sqrt(sum/((double) soln.n_elem));
	std::cout << "L2 norm of the error = " << norm << std::endl;

	test_solver.write_solution_additional("soln.dat",exact_soln,"exact");
	

  return 0;

}
