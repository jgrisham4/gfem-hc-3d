#include "shape_functions.h"
#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

int main() {
  
  // Declaring variables
	vector<double> xiv({-1.0,1.0,1.0,-1.0,-1.0,1.0,1.0,-1.0});
	vector<double> etav({-1.0,-1.0,1.0,1.0,-1.0,-1.0,1.0,1.0});
	vector<double> zetav({-1.0,-1.0,-1.0,-1.0,1.0,1.0,1.0,1.0});
	
	for (unsigned int i=0; i<xiv.size(); i++) {
		cout << "xi = " << xiv[i] << " eta = " << etav[i] << " zeta = " << zetav[i] << endl;
		cout << "N_" << i+1 << " = " << N(i,xiv[i],etav[i],zetav[i]) << endl;
	}
			

  return 0;

}
